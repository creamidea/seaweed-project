from head import *
# a = '8e7125'
# binascii.b2a_hex((binascii.a2b_hex(a))[::-1])
# '25718e'

class cl_frame_dealer():
    def __init__(self, handler):
        self.handler = handler
        self.main_frame = ''

    def f_send_beet(self, beet_frame):
        beet_pack = binascii.a2b_hex(beet_frame)
        self.handler.send(beet_pack)

    def f_send_time(self, time_frame):
        time = datetime.now().strftime('%y%m%d%H%M%S')
        time_frame += time
        time_pack = binascii.a2b_hex(time_frame)
        self.handler.send(time_pack)

    def f_recv_dev_id(self):
        cup = self.handler.recv(4)
        cup = cup[::-1]
        dev_id = binascii.b2a_hex(cup)
        self.main_frame += dev_id

    def f_beet_frame_solution(self, ):
        self.f_recv_dev_id()
        self.f_send_beet(self.main_frame)
        return self.main_frame

    def f_time_frame_solution(self, ):
        self.f_recv_dev_id()
        cup = self.handler.recv(1)
        cup = binascii.b2a_hex(cup)
        self.main_frame += cup
        if cup == d_time_frame_type['ACK']:
            self.f_send_time(self.main_frame[:-2])
            return 0
        else :
            return -1

    def f_pump_state_solution(self, n):
        d_pump = {}
        i = 1
        while 1 <= n:
            gump_id     = binascii.b2a_hex(self.handler.recv(1))
            gump_state  = binascii.b2a_hex(self.handler.recv(1))

            d_pump[gump_id] = gump_state
            self.main_frame += gump_id + gump_state
            i += 1

    class cl_node():
        def __init__(dev_id, pump_id, node_state, axy, temp):
            self.dev_id  = dev_id
            self.pump_id = pump_id
            self.node_state = node_state
            self.axy = axy
            self.temp = temp

    def f_node_data_solution(self, n):
        pass
        #d_node_list = {}
        #i = 1
        #while i < n:
            #dev_id      = binascii.b2a_hex(self.handler.recv(1))
            #pump_id     = binascii.b2a_hex(self.handler.recv(1))
            #node_state  = binascii.b2a_hex(self.handler.recv(1))
            #axy         = binascii.b2a_hex(self.handler.recv(2)[::-1])
            #temp        = binascii.b2a_hex(self.handler.recv(2)[::-1])
            #self.main_frame += dev_id + pump_id + node_state + axy + temp

            #node_msg = cl_node(dev_id, pump_id, int(node_state), (axy), temp)
            #d_node_list


    def f_state_frame_solution(self, ):
        self.f_recv_dev_id()

        dev_state = binascii.a2b_hex(self.handler.recv(1))
        len_data = binascii.a2b_hex(self.handler.recv(1))
        self.main_frame += dev_state + len_data

        num_pump = binascii.a2b_hex(self.handler.recv(1))
        self.main_frame += num_pump
        self.f_pump_state_solution(num_pump)

        num_node = binascii.a2b_hex(self.handler.recv(1))
        self.main_frame += num_node
        self.f_node_data_solution(num_node)

    def f_pack_main(self, handler):
        cup = binascii.b2a_hex(handler.recv(1))
        self.main_frame += cup
        if cup != d_frame_head['FRAME_H']:
            return 0
        cup = binascii.b2a_hex(handler.recv(1))
        self.main_frame += cup
        if cup != d_frame_head['FRAME_L']:
            return 0

        cup = binascii.b2a_hex(handler.recv(1))
        self.main_frame += cup
        if cup == d_frame_type['BEET']:
            self.f_beet_solution()
        elif cup == d_frame_type['TIME']:
            self.f_time_solution()
        elif cup == d_frame_type['STATE']:
            self.f_state_solution()
        else :
            return -1
