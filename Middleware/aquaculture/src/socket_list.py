# -*- coding: utf-8 -*-

from head import *
from frame_solution import cl_frame_dealer

class cl_sk_node():
    connection = '',
    last_time  = '',

class cl_sock_list():
    connections = {};
    msg_recv = cl_frame_dealer()
    def f_init_epoll(self):
        self.epoll = select.epoll()
        self.epoll.register(self.serversocket.fileno(), select.EPOLLIN)

    def f_init_serversocket(self):
        # serversocket在这边赋值
        self.serversocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.serversocket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.serversocket.bind(SERVER_ADDR)
        self.serversocket.listen(1)
        self.serversocket.setblocking(0)
        self.serversocket.setsockopt(socket.IPPROTO_TCP, socket.TCP_NODELAY, 1)

    def f_manage_list(self):
        try:
            while True:
                try:
                    while True:
                        events = self.epoll.poll(3)
                        for fileno, event in events:
                            if fileno == self.serversocket.fileno():
                                connection, address = self.serversocket.accept()
                                connection.setblocking(0)
                                self.epoll.register(connection.fileno(), select.EPOLLIN)

                                client = cl_sk_node()
                                #client.last_time = datetime.now().strftime("%Y%m%d%H%M%S")
                                client.last_time = datetime.now()
                                client.connection = connection

                                print "One connection established at %s " %(client.last_time)
                                client.connection.send("Welcome my friend, now you can chat\n")

                                self.connections[connection.fileno()] = client
                            elif event & select.EPOLLIN:
                                #print "in epollin ..."
                                result = self.msg_recv.f_receivemain(self.epoll, self.connections, fileno)
                                if result == -1:
                                    print "====== TIME OUT.Fileno %d client dead ~" %fileno
                                    self.f_dead_client_dealer(fileno)

                            elif event & select.EPOLLOUT:
                                result = self.msg_recv.f_sender(self.epoll, self.connections, fileno)

                            elif event & select.EPOLLHUP:
                                print "IN EPOLLHP, going to unregister and close fd: %d" %fileno
                                self.f_dead_client_dealer(fileno)

                            elif event & select.EPOLLERR:
                                print "In epoll error"
                                self.f_dead_client_dealer(fileno)

                except socket.error, e:
                    print "here1 in socket_error"
                    print e
                    continue
                    #break
                #except client_timeout, e:
                    #self.f_dead_client_dealer(fileno)

        except KeyboardInterrupt:
            print "keyboardinterrupted"
        except Exception, e:
            print e
            print "in exception"
        finally:
            self.epoll.unregister(self.serversocket.fileno())
            self.epoll.close()
            self.serversocket.close()

    def f_dead_client_dealer(self,fileno):
        self.epoll.unregister(fileno)
        self.connections[fileno].connection.close()
        del self.connections[fileno]

def main():
    c = cl_sock_list()
    c.f_init_serversocket()
    c.f_init_epoll()
    c.f_manage_list()

if __name__ == "__main__":
    main()

#!/usr/bin/env python

def main():
    pass
    
if __name__ == '__main__':
    main()
