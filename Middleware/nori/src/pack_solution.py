#!/usr/bin/env python
# -*- coding: utf-8 -*-
from head import *
from db_operator import *

#==============================================================
# This function is to send conmand of cdma online
#==============================================================
def send_command_cdma_online(handler):
    pack_command_cdma_online = ''
    pack_command_cdma_online += d_frame["FRAME_H"]
    pack_command_cdma_online += d_frame["FRAME_L"]
    pack_command_cdma_online += transfor(d_length["LEN_COMMAND"] + 2)
    pack_command_cdma_online += d_frame["FRAME_TYPE_COMMAND"]
    pack_command_cdma_online += d_command_type["CDMA_ONLINE"]
    pack_command_cdma_online += generate_check_byte(pack_command_cdma_online)

    handler.sendall(binascii.a2b_hex(pack_command_cdma_online))
    print "send cdma_online : %s done" % pack_command_cdma_online
    return d_return_mark["SEND_CDMA_ONLINE"]

#==============================================================
# This function is to send conmand of data OK
#==============================================================
def send_command_data_ok(handler):
    pack_command_data_ok = ''
    pack_command_data_ok += d_frame["FRAME_H"]
    pack_command_data_ok += d_frame["FRAME_L"]
    pack_command_data_ok += transfor(d_length["LEN_COMMAND"] + 2)
    pack_command_data_ok += d_frame["FRAME_TYPE_COMMAND"]
    pack_command_data_ok += d_command_type["DATA_OK"]
    pack_command_data_ok += generate_check_byte(pack_command_data_ok)

    handler.sendall(binascii.a2b_hex(pack_command_data_ok))
    print "send data_ok pack : %s done" % pack_command_data_ok
    return d_return_mark["SEND_DATA_OK"]

#==============================================================
# This function is to send data to update time on nodes
#==============================================================
def send_data_time(handler):
    now_time = datetime.now()
    now_time = now_time.strftime('%y%m%d%H%M%S')
    #now_time = now_time[2:14]
    i = 0
    s_now_time = ''
    while i <= 10:
        s_now_time += transfor(now_time[i: i + 2])
        i += 2

    frame_h         = d_frame["FRAME_H"]
    frame_l         = d_frame["FRAME_L"]
    #     len_time_pack = transfor(len(s_now_time) / 2 + 1 + 2)
    len_time_pack   = transfor(d_length["LEN_TIME_PACK"])
    frame_type_data = d_frame["FRAME_TYPE_DATA"]
    data_type_time  = d_frame["DATA_TYPE_TIME"]

    pack_data_time = ''
    pack_data_time += frame_h
    pack_data_time += frame_l
    pack_data_time += len_time_pack
    pack_data_time += frame_type_data
    pack_data_time += data_type_time
    pack_data_time += s_now_time
    pack_data_time += generate_check_byte(pack_data_time)

    handler.sendall(binascii.a2b_hex(pack_data_time))
    print "send time frame: %s " % pack_data_time
    return d_return_mark["SEND_UPDATE_DATATIME"]

#==============================================================
# This function is to check legality of the data in the frame
#==============================================================
def beet_solution(handler, beet, cup):
    print "recvING A BEET PACK..."
    beet += cup
    cup = handler.recv(d_length["LEN_BEET"] - len(beet))
    beet += cup
    return (beet, d_return_mark["BEET_PACK_READ_RIGHT"])

#==============================================================
# 此函数对帧进行帧校验
#==============================================================
def check_frame(handler, frame):
    print "checking data..."
#     frame_len = len(frame)
    try:
        check_byte = binascii.b2a_hex(frame[-1])
#         print "frame = %s  %r " % (frame, frame[-2])
        print "check_byte = " , check_byte
        result = generate_check_byte(binascii.b2a_hex(frame[:-1]))
        print "result = %s" %result
        if result == check_byte:
            print "************legal frame!!!"
            return d_return_mark["CHECK_LEGAL_FRAME"]
        else:
            print "!!!!!!!!!illegal frame!!!!!!!!!"
            return d_return_mark["CHECK_ILLEGAL_FRAME"]
    except IndexError as index_error:
        print index_error
        return d_return_mark["INDEX_ERROR"]

#==============================================================
# This function is to analyse and divide the frame
#==============================================================
def frame_solution(handler, frame, cup):
    print "GET A FRAME..."
    frame += cup
    cup = handler.recv(2)
    if binascii.b2a_hex(cup[1]) == d_frame["FRAME_TYPE_COMMAND"]:        #命令帧
        print "GOT A COMMAND..."
        frame += cup
        N = int(binascii.b2a_hex(cup[0]), 16)
        print "in function frame_solution, N = %d" % N
        cup = handler.recv(N - 2) ##############################
        if binascii.b2a_hex(cup) == d_command_type["DATA_OK"]:
            frame += cup
            print "GOT A DATA OK RESPONCE!"
            return d_return_mark["RECEIVE_DATA_OK"]
        elif binascii.b2a_hex(cup) == d_command_type["UPDATE_TIME"]:
            frame += cup
            print "GOT A ASK TO UPDATE TIME..."
            send_command_data_ok(handler)
            send_data_time(handler)
            return d_return_mark["RECEIVE_UPDATE_DATATIME"]
        else:
            return d_return_mark["UNKNOWN_COMMAND"]
    elif binascii.b2a_hex(cup[1]) == d_frame["FRAME_TYPE_DATA"]:      # 数据帧
        print "GOT A DATA PACK"
        frame += cup
        LEN = cup[0]  ################################################
        N = int(binascii.b2a_hex(LEN),16) - 2 - 1 #########################
        print "N = %d" % N
        cup = handler.recv(1)    # 取出Type字段
        if binascii.b2a_hex(cup) == d_frame["DATA_TYPE_DATA"]:     # 记录数据
            print "it's a record data!"
            i, z= 0, 0
            frame += cup
            while i < N:
                cup = handler.recv(d_length["LEN_INNER_HEAD"])
                print "the i = %d record" % z
                frame += cup
                j = 0
                len = int(binascii.b2a_hex(cup[2]),16)
                print len
                sensor_type = binascii.b2a_hex(cup[1])
                if sensor_type == d_sense_type["SENSE_TYPE_LIGHT"] :
                    while j < len:
                        cup = handler.recv(d_length["LEN_TIME"] + d_length["LEN_LIGHT"])
                        frame += cup
                        print "the j = %d inner_record" % j
                        print binascii.b2a_hex(cup)
                        j = j + 1
                    i += len * 10 + d_length["LEN_INNER_HEAD"]
                elif sensor_type == d_sense_type["SENSE_TYPE_MULTY"] :
                    while j < len:
                        cup = handler.recv(d_length["LEN_TIME"] + d_length["LEN_MULTY"])
                        frame += cup
                        print "the j = %d inner_record" %j
                        print binascii.b2a_hex(cup)
                        j = j + 1
                    i += len * 12 + d_length["LEN_INNER_HEAD"]
                else :
                    print "UNKNOW SENSOR TYPE! " + sensor_type
                    handler.recv(N)
                    return -1
                z += 1
#                 i = i + 1
            frame += handler.recv(1)
            return frame
        else:
            print "UNKNOWN_DATA_TYPE"
            return d_return_mark["UNKNOWN_DATA_TYPE"]
    else:
        print "UNKNOWN_FRAME_TYPE"
        return d_return_mark["UNKNOWN_FRAME_TYPE"]

#==============================================================
# This function is to check legality of the datas int the frame
#==============================================================
def check_store_data(frame):
    pack_num = int(binascii.b2a_hex(frame[2]), 16)
    #print "prck_num = %d" %pack_num
    #print "frame[4] = %s" %binascii.b2a_hex(frame[4])
    if binascii.b2a_hex(frame[4]) == d_frame["DATA_TYPE_DATA"]:
        p, q = 0, 0;
        data_part_solved_len = 0
        data_part = frame[5:]
        ###########################################
        # 迭代N组数据中的每一组
        ###########################################
        print "pack_num - 1 - 2 = %d " % (pack_num - 1 - 2)
        while q < pack_num - 1 - 2:  ###########################################################
            data_part = data_part[p:]
            print "in check_store_data, P = %d " % p
            node_id     = int(binascii.b2a_hex(data_part[0]), 16)
            sensor_set  = binascii.b2a_hex(data_part[1])
            data_num    = int(binascii.b2a_hex(data_part[2]), 16)
            print 'node_id = %d | sensor_type = %s | data_num = %d' %(node_id, sensor_set, data_num)
            k = 0
            data_circle = data_part[3:]
            ################################
            # 迭代检查每组数据中的每一笔记录
            ################################
            if sensor_set == d_sense_type["SENSE_TYPE_LIGHT"]:
                print '==================LIGHT========================='
                ##########################
                # 对光照数据解析部分
                ##########################
                while k < data_num:
                    temp_time = data_circle[10 * k : 10 * k + 6]
                    ## generating the datetime type data
                    sense_time = datetime(2000 + int(binascii.b2a_hex(temp_time[0]), 16),\
                            int(binascii.b2a_hex(temp_time[1]), 16), int(binascii.b2a_hex(temp_time[2]), 16), \
                            int(binascii.b2a_hex(temp_time[3]), 16), int(binascii.b2a_hex(temp_time[4]), 16), \
                            int(binascii.b2a_hex(temp_time[5]), 16))
                    sense_time = sense_time.strftime('%Y%m%d%H%M%S')

                    sense_time = datetime.now().strftime("%Y%m%d%H%M%S")

                    sense_data = data_circle[10 * k + 6 : 10 * k + 10]
                    sense_data = sense_data[::-1]           #取倒序数据
                    sense_data_light = int(binascii.b2a_hex(sense_data), 16)
                    print "here sense_data_light = %d" %sense_data_light
                    if sense_data_light >= 0 and sense_data_light <= 200000:
                        insert_data = t_insert_data()
                        insert_data.node_id     = node_id
                        insert_data.sensor_id   = d_sensor_id["LIGHT"]
                        insert_data.data        = sense_data_light
                        insert_data.insert_time = sense_time

                        db_op = db_operator()
                        db_op.db_connect()
                        ret = db_op.store_data(insert_data)
                        db_op.db_close()
                        if ret == d_return_mark["DB_STORE_OK"]:
                            print "store one data succeed!!!"
                        else :
                            print "ONE WRONG DATA BEEN PASSED!!!"
                    k += 1
                    print "in light data, k = %d" %k
                p = data_num * (d_length["LEN_LIGHT"] + d_length["LEN_TIME"]) + d_length["LEN_INNER_HEAD"]
            elif sensor_set == d_sense_type["SENSE_TYPE_MULTY"]:
                while k < data_num:
                    temp_time = data_circle[10 * k : 10 * k + 6]
                    ## generating the datetime type data
                    sense_time = datetime(2000 + int(binascii.b2a_hex(temp_time[0]), 16),\
                            int(binascii.b2a_hex(temp_time[1]), 16), int(binascii.b2a_hex(temp_time[2]), 16), \
                            int(binascii.b2a_hex(temp_time[3]), 16), int(binascii.b2a_hex(temp_time[4]), 16), \
                            int(binascii.b2a_hex(temp_time[5]), 16))
                    sense_time = sense_time.strftime('%Y%m%d%H%M%S')
                    sense_time = datetime.now().strftime("%Y%m%d%H%M%S")
                    ##########################
                    # 流速传感器节点数据解析部分
                    ##########################
                    sense_data = data_circle[12 * k + 6 : 12 * k + 12]

                    print "================TURMIDITY====================="
                    turmidity_sense_data = sense_data[0:2]
                    sense_data_turmidity = int(binascii.b2a_hex(turmidity_sense_data[::-1]), 16)
                    if sense_data_turmidity >= 0 and sense_data_turmidity <= 20000 and turmidity_sense_data != 'ffff':
                        ########################
                        #
                        ########################
                        sense_data_turmidity = float("%.3f" %(sense_data_turmidity / 100.0))
                        insert_data = t_insert_data()
                        insert_data.node_id     = node_id
                        insert_data.sensor_id   = d_sensor_id["HUMIDITY"]
                        insert_data.data        = sense_data_turmidity
                        insert_data.insert_time = sense_time

                        db_op = db_operator()
                        db_op.db_connect()
                        ret = db_op.store_data(insert_data)
                        db_op.db_close()
                        if ret != d_return_mark["DB_STORE_OK"]:
                            print "HERE2"
                    else :
                        print "WRONG RANGE OF TURMIDITY!!!"

                    print '=================FLOWSPEED======================'
                    flowspeed_sense_data = sense_data[2:4]
                    sense_data_flowspeed = int(binascii.b2a_hex(flowspeed_sense_data[::-1]), 16)
                    if sense_data_flowspeed >= 0 and sense_data_flowspeed <= 10000 and flowspeed_sense_data != 'ffff':
                        ########################
                        #
                        ########################

                        sense_data_flowspeed = float("%.3f" %(sense_data_flowspeed / 1000.0))
                        insert_data = t_insert_data()
                        insert_data.node_id     = node_id
                        insert_data.sensor_id   = d_sensor_id["FLOWSPEED"]
                        insert_data.data        = sense_data_flowspeed
                        insert_data.insert_time = sense_time

                        db_op = db_operator()
                        db_op.db_connect()
                        ret = db_op.store_data(insert_data)
                        db_op.db_close()
                        if ret != d_return_mark["DB_STORE_OK"]:
                            print "ONE WRONG DATA BEEN PASSED!!!"
                    else :
                        print "WRONG RANGE OF TURMIDITY!!!"

                    print "================TEMERATURE====================="
                    temp_sense_data = sense_data[4:6]
                    print "TEMPERATURE: %s" %(binascii.b2a_hex(temp_sense_data))
                    temp_sense_data = temp_sense_data[::-1]
                    high_16 = binascii.b2a_hex(temp_sense_data[0])
                    ##############################
                    # 温度解析部分，判断温度的正负
                    ##############################
                    if int(high_16[0], 16) == 0 and int(high_16[1], 16) <= 7:
                        print "================TEMPERATURE PASS ONE================= POSITIVE"
                        sense_data_temperature = int(binascii.b2a_hex(temp_sense_data), 16)
                        #### solution for positive numbers
                        sense_data_temperature *= 0.0625
                        if sense_data_temperature >= -55 and sense_data_temperature <= 125:
                            insert_data = t_insert_data()
                            insert_data.node_id     = node_id
                            insert_data.sensor_id   = d_sensor_id["TEMPERATURE"]
                            insert_data.data        = sense_data_temperature
                            insert_data.insert_time = sense_time

                            db_op = db_operator()
                            db_op.db_connect()
                            ret = db_op.store_data(insert_data)
                            db_op.db_close()
                            if ret != d_return_mark["DB_STORE_OK"]:
                                print "here temperature"
                        else:
                            print "[WARNING] TEMPERATURE OVER RANGE!!!!!"
                    elif int(high_16[0], 16) == 15 and int(high_16[1], 16) >= 8:
                        #### temperature is positive
                        print "================TEMPERATURE PASS ONE================= NEGATIVE"
                        sense_data_temperature = int(binascii.b2a_hex(temp_sense_data), 16)
                        #### solution for negative numbers
                        sense_data_temperature = -((sense_data_temperature ^ 65535 + 1) * 0.0625)
                        if sense_data_temperature >= -55 and sense_data_temperature <= 125:
                            insert_data = t_insert_data()
                            insert_data.node_id     = node_id
                            insert_data.sensor_id   = d_sensor_id["TEMPERATURE"]
                            insert_data.data        = sense_data_temperature
                            insert_data.insert_time = sense_time

                            db_op = db_operator()
                            db_op.db_connect()
                            ret = db_op.store_data(insert_data)
                            db_op.db_close()
                            if ret != d_return_mark["DB_STORE_OK"]:
                                print "sotre data error"
                        else:
                            print "[WARNING]TEMPERATURE OVER RANGE!!!!!"
                    else:
                        print "[ERROR]INLEGAL TEMPERATURE!!!!"
                    k += 1
                    print "in multy data, k = %d" %k
                p = data_num * (d_length["LEN_MULTY"] + d_length["LEN_TIME"]) + d_length["LEN_INNER_HEAD"]
            q += p
            print "The remain data_part : %s " % binascii.b2a_hex(data_part)
        else:
            return -1
    return 0


def pack_main(handler):
    try:
        handler.settimeout(3602)
        cup = handler.recv(1)
    except socket.timeout:
        print "in pack main time out"
        handler.shutdown(socket.SHUT_RD)
        handler.close()
        return -2
#     print binascii.b2a_hex(cup)
    if cup == beet_head[0]:
        beet = cup
        cup  = handler.recv(1)
        if cup == beet_head[1]:
            beet, flag = beet_solution(handler, beet, cup);
            print "BEET_PACK: " + beet
            if flag == d_return_mark["BEET_PACK_READ_RIGHT"] :
                send_command_cdma_online(handler)
            else:
                print "BEET PACK recv ERROR"
                return -1
        else:
            print "RECEIVE IRRIVALID CHARACTER"
            return -1
    elif binascii.b2a_hex(cup) == d_frame["FRAME_H"]:
        main_frame = cup
        cup = handler.recv(1)
        if binascii.b2a_hex(cup) == d_frame["FRAME_L"]:
            try:
                main_frame = frame_solution(handler, main_frame, cup)
                if main_frame == d_return_mark["RECEIVE_UPDATE_DATATIME"] \
                        or main_frame == d_return_mark["RECEIVE_DATA_OK"] \
                        or main_frame == d_return_mark["UNKNOWN_COMMAND"] \
                        or main_frame == d_return_mark["UNKNOWN_DATA_TYPE"] \
                        or main_frame == d_return_mark["UNKNOWN_FRAME_TYPE"] :
                    return -1
                else:
                    print "DATE_RECORD_FRAME: " + binascii.b2a_hex(main_frame) ##########################
                    flag = check_frame(handler, main_frame)
                    if flag != d_return_mark["CHECK_LEGAL_FRAME"]:
                        return -1
                    check_store_data(main_frame)
                    print "storing data..."
                    send_command_data_ok(handler)
            except IndexError as index_error:
                print "IndexError: string index out of range"
                print index_error
                return -1
            else:
                print "One frame data solved successfully with no error occuring! "
                print "------------------------------------------------------------------------"
                print "NOW NEXT..."
                return -1
        else:
            return -1
    else:
#         print "unknow cup cup : %s" %binascii.b2a_hex(cup)
#         sleep(1)
        return -1
