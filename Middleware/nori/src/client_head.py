#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import socket
import binascii
from  time import sleep
from pack_solution import *
from db_operator import *
from head import *
from pack_solution import (generate_check_byte, transfor)
from head import (HOST, PORT)

# Create a socket (SOCK_STREAM means a TCP socket)
sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

pack_beet = beet_head + "18015621211"
################################################
# generate command pack
################################################
pack_frame_command = ""
pack_frame_command += d_frame["FRAME_H"]
pack_frame_command += d_frame["FRAME_L"]
pack_frame_command += transfor(2 + 2)
pack_frame_command += d_frame["FRAME_TYPE_COMMAND"]

pack_data_ok        = pack_frame_command
pack_data_ok        += d_command_type["DATA_OK"]
pack_data_ok        += generate_check_byte(pack_data_ok)

pack_update_time    = pack_frame_command
pack_update_time    += d_command_type["UPDATE_TIME"]

pack_update_time    += generate_check_byte(pack_update_time)

################################################
# generate data pack
################################################

pack_light = transfor(1) # set the device number
pack_light += d_sense_type["SENSE_TYPE_LIGHT"]
pack_light += transfor(1) # set the number of light data
tm = get_time()
pack_light += tm
pack_light += "04030201"

pack_multy = transfor(1) # set the device number
pack_multy += d_sense_type["SENSE_TYPE_MULTY"]
pack_multy += transfor(1) # set the number of multy data
tm = get_time()
pack_multy += tm
pack_multy += "0201"    # set the flow_speed data
pack_multy += "0403"    # set the turimidity data
pack_multy += "0504"    # set the temperature

pack_frame_data = ''
pack_frame_data += d_frame["FRAME_H"]
pack_frame_data += d_frame["FRAME_L"]
len_data = (len(pack_light) + len(pack_multy)) / 2
pack_frame_data += transfor(len_data + 1 + 2)  # 1: 节点类型    2: 数据标志和校验
pack_frame_data += d_frame["FRAME_TYPE_DATA"]
pack_frame_data += d_frame["DATA_TYPE_DATA"]

pack_frame_data += pack_light
pack_frame_data += pack_multy

#pack_frame_data = "67711202020104010D07030C371E020104030504"
complex_frame = "67711202020104010D09190C0000F4011B009101"
pack_frame_data = "67711002020203010D07030C371E04030200"
pack_frame_data += generate_check_byte(pack_frame_data)
complex_frame += generate_check_byte(complex_frame)

# print (len(binascii.a2b_hex(pack_frame_data)) - 3)
#==============================================================
# 此函数将一个字节能表示的十六进制数格式化，遇到16以内的数：0～f， 格式化为‘0+x’，其他的不变
# 返回格式化后的字符串
#==============================================================
def transfor(one_Byte):
    temp = int(one_Byte)
    #if temp > 0 and temp < 16:
    if temp < 16:
        temp = hex(temp)[2:3]
        temp = '0' + temp
    #else temp >= 16 :
    else:
        temp = hex(temp)[2:4]
    #else:
        #return ''
    return temp


def test():
    print "pack_beet        : ", pack_beet
    print "pack_data_ok     : ", pack_data_ok
    print "pack_update_time : ", pack_update_time
    print "pack_frame_data  : ", pack_frame_data

if __name__ == "__main__":
    test()
