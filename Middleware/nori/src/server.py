#!/usr/bin/env python
# -*- coding: utf-8 -*-
# import SocketServer
from pack_solution import *
import threading
import socket
import thread

class MyThread(threading.Thread):
    def __init__(self, func, args, name=''):
        threading.Thread.__init__(self)
        self.name = name
        self.func = func
        self.args = args

    def getResult(self):
        return self.res

    def run(self):
        print "one connection established!!!"
        print 'STARTING', self.name, 'AT:', datetime.now()
        while 1:
            try:
                self.args[0].settimeout(3602)
                self.res = apply(self.func, self.args)

                # 鐏忓棜绉撮弮鑸垫闂傜顔曠純顔昏礋5
#                 buf = self.args[0].recv(8196)
#                 if len(buf) != 0:
#                     print buf
            except socket.timeout:
                print "time out ---- ",
                print self.name, 'FINISHED AT:', datetime.now()
                break
            except Exception, e :
                print e
                print self.name, 'FINISHED AT:', datetime.now()
                break

class thread_manager(object):
    thread_list = []

    def init_thread_list(self):
        self.thread_list.append( MyThread(self.downword,(), self.downword.__name__) )
        self.thread_list.append( MyThread(self.upword,(), self.upword.__name__) )

    def start_thread(self):
        thread_num = len(self.thread_list)
        for i in range(thread_num):
            self.thread_list[i].start()

def solu(clientsock):
    print "in solu"

def server_main():
    db_op = db_operator()
    db_op.db_connect()
    db_op.select_cur.execute("select version()")
    vers = db_op.select_cur.fetchone()
    print "database version is: %s" %vers
    db_op.db_close()

    s = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
    s.bind((HOST, PORT))
    thread_mag = thread_manager()

    print "bef listen"
    s.listen(5)
    try:
        while 1:
            print "in while"
            clientsock,clientaddr = s.accept()
            conn = MyThread(pack_main, (clientsock, ))
            thread_mag.thread_list.append(conn)
            conn.start()
#             conn.join()
    except KeyboardInterrupt as keyboard_interrupt:
        print "[KeyboardInterrupt] program is going to down...Thank you!!! "
#         clientsock.shutdown(socket.SHUT_RD)
#         clientsock.close()
        s.shutdown(socket.SHUT_RDWR)
        s.close()
        if db_op.handler:
            db_op.handler.close()
        #sys.exit(1)
    finally:
        print "For any advice and any question, please contact us(WSNG):"
        print "\t\tcow.vangogh@gmail.com(liupeng) \n \t\tcreamidea@gmail.com(nijunjia) \n \t\tjohnchain.li@gmail.com(lijunqian)"




# class MyTCPHandler(SocketServer.BaseRequestHandler):
#     """
#     The RequestHandler class for our server.
#     It is instantiated once per connection to the server, and must
#     override the handle() method to implement communication to the
#     client.
#     """
#
#     def handle(self):
#         # self.request is the TCP socket connected to the client
#         cur_thread = threading.current_thread()
# #         while True:
#         print "hello here, thread id = %s" % cur_thread.name
#         try:
#             while True:
#                 n = 0
#                 n = pack_main(self.request)
#                 if n == -2:
#                     raise socket.error
# #                     return -2
#         except socket.error, e:
#             print "socket error " , e
#         except Exception, e :
#             print e
#         finally:
#             print "<<<<< The thread %s is going to exit!!! " % cur_thread.name
#
# class ThreadedTCPServer(SocketServer.ThreadingMixIn, SocketServer.TCPServer):
#     pass

# def main():
#     db_op = db_operator()
#     db_op.db_connect()
#     db_op.select_cur.execute("select version()")
#     vers = db_op.select_cur.fetchone()
#     print "database version is: %s" %vers
#     db_op.db_close()
#     SocketServer.TCPServer.allow_reuse_address = True
#
#     # Create the server, binding to localhost on port 9999
# #     server = SocketServer.TCPServer((HOST, PORT), MyTCPHandler)
#     server = ThreadedTCPServer((HOST, PORT), MyTCPHandler)
#     # Activate the server; this will keep running until you
#     # interrupt the program with Ctrl-C
#
#     try:
#
#         server_thread = threading.Thread(target=server.serve_forever)
#         # Exit the server thread when the main thread terminates
#         server_thread.daemon = True
#         server_thread.start()
#         print "hello here1"
#         server_thread.join()
#         print "hello here2"
#     except KeyboardInterrupt as keyboard_interrupt:
#         print "[KeyboardInterrupt] program is going to down...Thank you!!! "
#         server.shutdown()
#         if db_op.handler:
#             db_op.handler.close()
#         #sys.exit(1)
#     finally:
#         print "For any advice and any question, please contact us(WSNG):"
#         print "\t\tcow.vangogh@gmail.com(liupeng) \n \t\tcreamidea@gmail.com(nijunjia) \n \t\tjohnchain.li@gmail.com(lijunqian)"


if __name__ == "__main__":
    server_main()

