Django==1.5.4
Jinja2==2.7.1
MarkupSafe==0.18
MySQL-python==1.2.3
Pygments==1.6
Pymacs==0.25
Sphinx==1.1.3
docutils==0.11
ipython==1.1.0
matplotlib==1.3.1
numpy==1.7.1
pyflakes==0.7.3
pyreadline==2.0
python-memcached==1.53
python-mode.el==6.1.2
rope==0.9.4
ropemacs==0.7
ropemode==0.2
scipy==0.13.0
virtualenv==1.10.1
