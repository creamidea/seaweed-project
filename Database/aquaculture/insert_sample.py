from db_operator import DbOperator

def inser_producted_node(dop):
    for i in range(1, 101):
        sql_str = r"insert into tb_node_producted values(%d)" %(i)
        dop.insert_cur.execute(sql_str)
        dop.handler.commit()

def insert_user(dop):
    for i in range(1, 101):
        sql_str = r'''insert into tb_user(user_id, pwd, phone, name) values(%d, '%s', '%s', '%s')''' %(i, '111111', '110', 'jason')
        dop.insert_cur.execute(sql_str)
        dop.handler.commit()

def insert_pond(dop):
    for i in range(1, 101):
        for j in range(1, 11):
            sql_str = r''' insert into tb_pond(user_id, pond_name) values(%d, '%s')''' %(i, 'pond_name')
            dop.insert_cur.execute(sql_str)
            dop.handler.commit()

def insert_eq(dop):
    for i in range(1, 101):
        sql_str = r'''insert into tb_eq(eq_id, user_id) values(%d, %d)''' %(i, i)
        dop.insert_cur.execute(sql_str)
        dop.handler.commit()

def insert_pump(dop):
    for i in range(1, 101):
        for j in range(1, 3):
            sql_str = r'''insert into tb_pump(eq_id, pump_id, pump_state) values(%d, %d, %d)''' %(i, j, 0)
            dop.insert_cur.execute(sql_str)
            dop.handler.commit()

def insert_pump_pond(dop):
    for i in range(1, 101):
        for j in range(1, 11):
            for k in range(1, 3):
                sql_str = r'''insert into tb_pump_pond values(%d, %d, %d)''' %(i, j, k)
                dop.insert_cur.execute(sql_str)
                dop.handler.commit()

def insert_node(dop):
    for i in range(1, 101):
        sql_str = r'''insert into tb_node(node_id, node_state, node_name, eq_id, user_id) values(%d, %d, '%s', %d, %d)''' %(i + 200, 0, 'icecream', i, i)
        dop.insert_cur.execute(sql_str)
        dop.handler.commit()

def insert_data(dop):
    import datetime
    from random import random
    internal = datetime.timedelta(minutes = 30)
    for i in range(1, 101):
        for j in range(1, 11):
            for k in range(1, 100):
                oxy     = int(random() * 100)
                temp    = int(random() * 100)
                start   = datetime.datetime.now()
                record_time = (start + internal * k).strftime('%Y%m%d%H%M%S')
                sql_str = r'''insert into tb_data(node_id, pond_id, record_time, oxy, temp) values(%d, %d, '%s', %d, %d)''' %(i, j, record_time, oxy, temp)
                dop.insert_cur.execute(sql_str)
                dop.handler.commit()

dop = DbOperator()
dop.db_connect()
print "Database connected "

inser_producted_node(dop)
print "producted node inserted"
insert_user(dop)
print "user inserted "
insert_pond(dop)
print "pond inserted"
insert_eq(dop)
print "eq inserted"
insert_pump(dop)
print "pump inserted"
insert_pump_pond(dop)
print "pump_pond inserted"
insert_node(dop)
print "node inserted"
insert_data(dop)
print "data inserted"

dop.db_close()
print "db closed"
