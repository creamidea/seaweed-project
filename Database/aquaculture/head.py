#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys
import MySQLdb
import binascii
import socket, select
from time import sleep
from datetime import datetime

DEADTIME = 3
SERVER_ADDR = ('0.0.0.0', 9999)
EPOLL_TIMEOUT = 3

d_db_conn_local = {
    "HOST"      : "localhost",
    "USER"      : "djangouser",
    "PASSWORD"  : "djangouser",
    #"DATABASE"  : "irrigating_system",
    "DATABASE"  : "djangodb2",
    }

messages = {}

d_frame_head = {
                'FRAME_H' : '7e',
                'FRAME_L' : '81',
                }
d_frame_type = {
                'BEET'      : '00',
                'TIME'      : '01',
                'STATE'     : '03',
                }

d_time_frame_type = {
                'ACK'       : '01',
                'SET'       : '02',
                }

d_dev_state = {
                'AUTO'      : '00',
                'MENUAL'    : '01',
                }

d_node_state = {
                'ONLINE'    : '00',
                'OFFLINE'   : '01',
                'POWERLESS' : '02',
                }
d_pump_state = {
                'OFF'       : '00',
                'ON'        : '01',
                }

