#!/usr/bin/env python
# -*- coding: utf-8 -*-

from head import *
from datetime import date

class DbOperator():
    def db_connect(self):
        d_db_conn = d_db_conn_local
        self.handler = MySQLdb.connect(d_db_conn["HOST"], \
            d_db_conn["USER"], d_db_conn["PASSWORD"], d_db_conn["DATABASE"])

        self.select_cur = self.handler.cursor()
        self.update_cur = self.handler.cursor()
        self.insert_cur = self.handler.cursor()
        return self.handler, self.select_cur, self.update_cur, self.insert_cur

    def db_init(self):
        #try:
        if 1:
            for line in open(MYSQL_SQL_PATH):
                #self.update_cur.execute(line)
            #self.handler.commit()
                print "^^^^^^^^^^^^^^^^^"
                print line
        #except sqlite3.OperationalError, e:
            #print e

    def db_close(self):
        if self.handler:
            self.select_cur.close()
            self.insert_cur.close()
            self.update_cur.close()
            self.handler.close()
    ###################################################################


    def db_s_user_all_pond(self, user_id):
        print "here1"
        sql_str = r'''
                    select pond_id, pond_name from tb_pond
                    where user_id = '%s'
                    ''' %(user_id)
        result = self.select_cur.execute(sql_str)
        return self.select_cur

    def db_s_check_node_legal(self, node_id):
        print "here2"
        sql_str = r'''
                    select count(node_id) from tb_node_producted
                    where node_id = %d
                    ''' %(node_id)
        result = self.select_cur.execute(sql_str)
        num = self.select_cur.fetchall()
        if num[0] == 0:
            return -1
        else:
            return 0
        #return self.select_cur

    def db_s_certain_pond_node(self, user_id, pond_name):
        print "here3"
        sql_str = r'''
                    select node_id, node_name, node_state from tb_node left join tb_pond using (user_id)
                    where user_id = %d and pond_name = '%s'
                    ''' %(user_id, pond_name)
        result = self.select_cur.execute(sql_str)
        return self.select_cur

    def db_s_certain_node_state(self, user_id, node_name):
        print "here4"
        sql_str = r'''
                    select node_state from tb_node
                    where user_id = %d and node_name = '%s'
                    ''' %(user_id, node_name)
        result = self.select_cur.execute(sql_str)
        return self.select_cur

    def db_s_certain_pump_state(self, user_id, pump_id):
        print "here5"
        sql_str = r'''
                    select pump_state from tb_user, tb_eq, tb_pump
                    where tb_user.user_id = %d and tb_eq.eq_id = tb_pump.eq_id and tb_pump.pump_id = %d
                    ''' %(user_id, pump_id)
        result = self.select_cur.execute(sql_str)
        return self.select_cur

    def db_s_certain_pond_pump(self, user_id, pond_name):
        print "here6"
        result = self.select_cur.callproc('s_certain_pond_pump',[user_id, pond_name])
        return self.select_cur


    def db_s_certain_pond_all_realtime_data(self, user_id, pond_name):
        print "here7"
        result = self.select_cur.callproc('s_certain_pond_all_realtime_data', (user_id, pond_name))
        return self.select_cur

    def db_s_certain_pond_today_data(self, user_id, pond_name):
        print "here8"
        start_time = date.today().strftime('%Y%m%d') + '000000'
        end_time = datetime.now().strftime('%Y%m%d%H%M%S')
        print "start_time = %s, end_time = %s" %(start_time,end_time)
        result = self.select_cur.callproc('s_certain_pond_time_range_data', (user_id, pond_name,start_time, end_time))
        return self.select_cur

    def db_s_certain_pond_time_range_data(self, user_id, pond_name, start_time, end_time):
        print "here9"
        result = self.select_cur.callproc('s_certain_pond_time_range_data', [user_id, pond_name,start_time, end_time])
        return self.select_cur


    def db_i_registe(self, user_id, pond_name,node_id):
        print "here10"
        result = self.select_cur.callproc('i_registe', (user_id, pond_name, node_id))
        self.handler.commit()
        return self.select_cur

    def db_u_set_pond_name(self, user_id, orign_name, new_name):
        print "here11"
        sql_str = r'''
                    update tb_pond
                    set pond_name = '%s'
                    where user_id = %d and pond_name = '%s'
                    ''' %(new_name, user_id, orign_name)
        result = self.select_cur.execute(sql_str)
        self.handler.commit()
        return self.select_cur

def main():
    test = DbOperator()
    test.db_connect()
    test.select_cur.execute("select version()")
    vers = test.select_cur.fetchone()
    print "database version is: %s" %vers

    cur = test.db_s_user_all_pond(1)
    cur.fetchall()
    cur = test.db_s_check_node_legal(1)
    print cur
    cur = test.db_s_certain_pond_node(1, '')
    cur.fetchall()
    cur = test.db_s_certain_node_state(1, '')
    cur.fetchall()
    cur = test.db_s_certain_pump_state(1, 1)
    cur.fetchall()
    test.db_close()
    test.db_connect()
    cur = test.db_s_certain_pond_pump(1, '')
    cur.fetchall()
    test.db_close()
    test.db_connect()
    cur = test.db_s_certain_pond_all_realtime_data(1, '')
    cur.fetchall()
    test.db_close()
    test.db_connect()
    cur = test.db_s_certain_pond_today_data(1, 'yu')
    cur.fetchall()
    test.db_close()
    test.db_connect()
    cur = test.db_s_certain_pond_time_range_data(1, 'yu', '000000000000', '000000000000')
    cur.fetchall()
    test.db_connect()
    cur = test.db_i_registe(1, 'yu', 1)
    cur.fetchall()
    cur = test.db_u_set_pond_name(1, 'yu', 'fish')
    cur.fetchall()
    test.db_close()

if __name__ == "__main__":
    main()
