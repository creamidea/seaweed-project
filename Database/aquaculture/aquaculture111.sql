-- -- DROP database IF EXISTS aquaculture;
-- -- CREATE database IF NOT EXISTS aquaculture;
-- -- USE aquaculture;
USE djangodb2;

DROP TABLE IF EXISTS tb_node_producted;
CREATE TABLE IF NOT EXISTS tb_node_producted(
    node_id INTEGER PRIMARY KEY
    );

DROP TABLE IF EXISTS tb_user;
CREATE TABLE IF NOT EXISTS tb_user(
    user_id     INTEGER NOT NULL,
    pwd         VARCHAR(200) NOT NULL,
    phone       VARCHAR(15),
    name        varchar(50),
    back1       INTEGER DEFAULT NULL,
    back2       varchar(200) DEFAULT NULL,
    PRIMARY KEY (user_id)
    );

DROP TABLE IF EXISTS tb_pond;
CREATE TABLE IF NOT EXISTS tb_pond(
    pond_id     INTEGER AUTO_INCREMENT,
    user_id     INTEGER NOT NULL,
    pond_name   VARCHAR(50),
    back1       INTEGER DEFAULT NULL,
    PRIMARY KEY (pond_id)
    /*CONSTRAINT pond_uid_dc FOREIGN KEY (user_id) REFERENCES tb_user(user_id) ON update no action */
    );

DROP TABLE IF EXISTS tb_eq;
CREATE TABLE IF NOT EXISTS tb_eq(
    eq_id       INTEGER NOT NULL ,
    user_id     INTEGER NOT NULL,
    back1       INTEGER DEFAULT NULL,
    PRIMARY KEY (eq_id, user_id),
    CONSTRAINT eq_uid_dc FOREIGN KEY (user_id) REFERENCES tb_user(user_id) ON DELETE CASCADE
    );

DROP TABLE IF EXISTS tb_pump;
CREATE TABLE IF NOT EXISTS tb_pump(
    eq_id       INTEGER NOT NULL,
    pump_id     INTEGER NOT NULL,
    pump_state  INTEGER DEFAULT 0,
    back1       INTEGER DEFAULT NULL,
    /*Time when the state of the pump updated*/
    time        TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,           
    PRIMARY KEY (eq_id, pump_id),
    CONSTRAINT pump_eid_dc FOREIGN KEY (eq_id) REFERENCES tb_eq(eq_id) ON DELETE CASCADE
    );

DROP TABLE IF EXISTS tb_pump_pond;
CREATE TABLE IF NOT EXISTS tb_pump_pond(
    user_id     INTEGER NOT NULL,
    pond_id     INTEGER NOT NULL,
    pump_id     INTEGER NOT NULL,
    PRIMARY KEY (user_id, pond_id, pump_id),
    CONSTRAINT pp_puid_dc   FOREIGN KEY (pond_id) REFERENCES tb_pond(pond_id) ON DELETE CASCADE
    /*CONSTRAINT puid      FOREIGN KEY (pump_id) REFERENCES tb_pump(pump_id)*/
    );

DROP TABLE IF EXISTS tb_node;
CREATE TABLE IF NOT EXISTS tb_node(
    node_id     INTEGER PRIMARY KEY NOT NULL,
    node_state  INTEGER DEFAULT 0,
    node_name   VARCHAR(50), 
    eq_id       INTEGER DEFAULT NULL,
    pond_id     INTEGER DEFAULT NULL,
    pump_id     INTEGER DEFAULT NULL,
    user_id     INTEGER DEFAULT NULL,
    back1       INTEGER DEFAULT NULL,
    CONSTRAINT node_eid_dc FOREIGN KEY (eq_id) REFERENCES tb_eq(eq_id) ON DELETE CASCADE
    /*CONSTRAINT */
    );

DROP TABLE IF EXISTS tb_data;
CREATE TABLE IF NOT EXISTS tb_data(
    instance_id INTEGER PRIMARY KEY NOT NULL,
    node_id     INTEGER NOT NULL,
    pond_id     INTEGER NOT NULL,
    record_time DATETIME NOT NULL,
    oxy         REAL    NOT null,
    temp        REAL    NOT null,
    back1       REAL    DEFAULT NULL,
    back2       INTEGER DEFAULT NULL,
    CONSTRAINT data_nid_dc FOREIGN KEY (node_id) REFERENCES tb_node(node_id) ON DELETE CASCADE
    );

drop table if EXISTS tmp_data;
CREATE TABLE IF NOT EXISTS tmp_data(
    node_id     INTEGER NOT NULL,
    pond_id     INTEGER NOT NULL,
    record_time DATETIME NOT NULL,
    oxy         REAL    NOT null,
    temp        REAL    NOT null
    );

    /*create or replace view tmp_data as*/
    

DROP TABLE IF EXISTS tb_operation;
CREATE TABLE IF NOT EXISTS tb_operation(
    op_id       INTEGER PRIMARY KEY NOT NULL,
    user_id     INTEGER NOT NULL,
    eq_id       INTEGER NOT NULL, 
    pump_id     INTEGER NOT NULL,
    pond_id     INTEGER NOT NULL,
    act         INTEGER NOT NULL,
    time        DATETIME NOT NULL,
    back1       varchar(200) DEFAULT NULL,
    CONSTRAINT opera__uid_dn FOREIGN KEY (user_id) REFERENCES tb_user(user_id) ON DELETE NO ACTION,
    CONSTRAINT opera_eid_dn FOREIGN KEY (eq_id) REFERENCES tb_eq(eq_id)       ON DELETE NO ACTION
    );

drop index idx_tb_user_name on tb_user;
CREATE INDEX idx_tb_user_name on tb_user(
    name
);

drop index idx_tb_data on tb_data;
CREATE INDEX idx_tb_data on tb_data(
    node_id,
    pond_id, 
    record_time
);

drop index idx_tb_pump on tb_pump;
CREATE INDEX idx_tb_pump on tb_pump(
    eq_id,
    pump_id
);

drop index idx_tb_operation on tb_operation;
CREATE INDEX idx_tb_operation on tb_operation(
    user_id,
    eq_id,
    pump_id,
    time
);

select "here";

DELIMITER ;;

-- 在插入tb_data之前，需要将数据插入临时表，即使实时数据表
DROP TRIGGER IF EXISTS trg_data_bef_insert;;
CREATE TRIGGER trg_data_bef_insert
    BEFORE INSERT ON tb_data
    FOR EACH ROW
BEGIN
    declare amount INTEGER;
    set amount = 0;
    select count(node_id) from tmp_data
        where node_id = new.node_id
        into amount;
    if amount = 0 then
        INSERT INTO tmp_data VALUES(new.node_id, new.pond_id, new.record_time, new.oxy, new.temp);
    else
        update tmp_data
        set record_time = new.record_time,
            oxy = new.oxy, 
            temp = new.temp
        where node_id = new.node_id and pond_id = new.pond_id;
    end if;
end;;

-- 确定池塘与泵的关系。
-- 左连接是因为数据在不同的表中
drop procedure if exists s_certain_pond_pump;;
CREATE procedure s_certain_pond_pump(in u_id integer , in pond_name varchar(50))
begin
    declare temp_pond_id integer;
    select pond_id from tb_pond
    where tb_pond.user_id = user_id and tb_pond.pond_name = pond_name limit 1 into temp_pond_id;
    
    select pump_id, pump_state from tb_pump_pond left join tb_pump using(pump_id)
    where user_id = u_id and pond_id = temp_pond_id ;

end;;

-- 查出一个池塘对应的所有采集数据
drop procedure if exists s_certain_pond_all_realtime_data;;
create procedure s_certain_pond_all_realtime_data(in user_id integer, in pond_name varchar(50))
begin
    declare temp_pond_id integer;
    select pond_id from tb_pond
    where tb_pond.user_id = user_id and tb_pond.pond_name = pond_name limit 1 into temp_pond_id;

    select node_id, oxy, temp, record_time from tmp_data left join tb_node using(node_id, pond_id)
    where user_id = user_id and pond_id = temp_pond_id ;
end;;

-- 查出给定起始/终止时间内池塘的采集数据
drop procedure if exists s_certain_pond_time_range_data;;
create procedure s_certain_pond_time_range_data(in user_id integer, in pond_name varchar(50), in start_time varchar(20), in end_time varchar(20))
begin
    declare temp_pond_id integer;
    set @id         = user_id;
    set @name       = pond_name;
    set @start_time = start_time;
    set @end_time   = end_time;

    select pond_id from tb_pond
    where tb_pond.user_id = user_id and tb_pond.pond_name = pond_name limit 1 into temp_pond_id;

    -- 这个是mysql的预处理语法？
    prepare a1 from 'select node_id, oxy, temp, record_time from tb_data left join tb_node using(node_id)
    where user_id = ? and tb_data.pond_id = ? and record_time >= ? and record_time <= ?';

    execute a1 using @id, @name, @start_time, @end_time;
end;;

-- 这里是注册的存储过程，将节点编号和池塘名称绑定
-- in表示输入参数
drop procedure if exists i_registe;
create procedure i_registe(in u_id integer, in p_name varchar(50), in n_id integer)
begin
    declare temp_pond_id    integer;
    declare num_pond        integer;
    declare num_node        integer;
    
    -- 查出该池塘名称是否存在
    -- 如果存在则为1，并赋值temp_pond_id用于后面插入tb_node
    -- 否则为0
    select count(pond_id), pond_id from tb_pond
    where user_id = u_id and pond_name = p_name
    into num_pond, temp_pond_id;

    -- 查出该节点编号是否存在，即判断该节点号是否合法
    -- 如果存在则num_node为1，即节点编码有效为1
    -- 否则为0
    -- select count(node_id) from tb_node
    -- where node_id = n_id
    -- into num_node;
    select count(node_id) from tb_node_producted
    where node_id = n_id
    into num_node;

    -- 如果一个池塘也没有说明这是用户创建的新池塘
    -- 则要将其插入tb_pond表，并赋值temp_pond_id
    if num_pond = 0 then
        insert into tb_pond(user_id, pond_name) values(u_id, p_name);
        select pond_id from tb_pond 
        where user_id = u_id and pond_name = p_name
        into temp_pond_id;
    end if;

    -- 节点编号有效时执行
    -- 插入tb_node表中
    -- 同时删去tb_node_producted中的node_id说明该节点号被注册，从此无效
    if num_node = 1 then
        insert into tb_node(node_id, user_id, pond_id) values(n_id, u_id, temp_pond_id);
        delete from tb_node_producted where node_id = n_id;
    else
        -- 01 is warning 
        SIGNAL SQLSTATE '01000'
        SET MESSAGE_TEXT = 'node_id is invalid', MYSQL_ERRNO = 1000;
    end if;
end;;

DELIMITER ;
