#!/bin/bash
# Program:
# Create a database suitable for this project in one step!
# Backup the database if it already exists and then clear the database.
# History:
# 2012/12/18

#配置参数
host=127.0.0.1                     #数据库服务器IP
admin=root                         #数据库管理员
admin_password=njjizyj0826             #数据库用户密码
user=smartwin                      #普通用户
user_password=smartwin               #普通用户密码
db_name=smartwin_cdma              #数据库名称

RELA_PATH=`pwd`
#如果tinyos数据库已存在则对其备份
# echo "BACKING UP DATABASE $db_name"
#sh db_backup.sh

# echo "BEGIN CHECK DATABASE"
# #如果$db_name数据库已存在，则删除
# echo "DROP   DATABASE IF EXISTS $db_name"     | mysql -u $admin --password=$admin_password -h $host
# #如果$db_name数据库不存在，则创建
# echo "CREATE DATABASE IF NOT EXISTS $db_name" | mysql -u $admin --password=$admin_password -h $host

# #创建用户，并对其权限做设置
# echo "CREATE USER IF NOT EXIST"
# echo "grant all privileges on $db_name.* to $user@'%' identified by '$user_password'" \
#                                               | mysql -u $admin --password=$admin_password -h $host

#初始化数据库
echo "BEGIN INIT DATABASE..."
echo "CREATING TABLES..."
cat ./sql/db_creater.sql | mysql -u $admin --password=$admin_password -h $host $db_name 
# echo "CREATING INDEX..."
# cat ./sql/index_creater.sql | mysql -u $admin --password=$admin_password -h $host $db_name
# echo "CREATING VIEWS..."
# cat ./sql/view_creater.sql | mysql -u $admin --password=$admin_password -h $host $db_name 
# echo "CREATING TRIGGER..."
# cat ./sql/trigger_creater.sql | mysql -u $admin --password=$admin_password -h $host $db_name 
# echo "CREATING PROCEDURE..."
# cat ./sql/procedure_creater.sql | mysql -u $admin --password=$admin_password -h $host $db_name 
echo "CREATE $db_name SUCCESS !"

#插入示例数据
#echo "FILLING SAMPLE_DATA..."
#cat  db_data_fill.sql | mysql -u $admin --password=$admin_password -h $host $db_name 
#echo "FILLING SAMPLE_DATA FINISHED!"
