-- drop database if exists smartwin_cdma ;
-- create database smartwin_cdma;
use smartwin_cdma;

drop table if exists tb_history_data;
CREATE TABLE if not exists tb_history_data ( 
    record_id   INTEGER  PRIMARY KEY AUTO_INCREMENT NOT NULL UNIQUE,
    insert_time DATETIME NOT NULL,
    node_id     INTEGER  NOT NULL,
    sensor_id   INTEGER  NOT NULL,
    data        REAL     NOT NULL DEFAULT  -1000  
);

drop table if exists tb_realtime_data;
CREATE TABLE if not exists tb_realtime_data ( 
    --  Django need this column id
    -- id integer AUTO_INCREMENT NOT NULL PRIMARY KEY,
    node_id     INTEGER  NOT NULL,
    sensor_id   INTEGER  NOT NULL,
    data        REAL     NOT NULL DEFAULT  -1000 ,
    insert_time DATETIME NOT NULL,
    PRIMARY KEY ( node_id, sensor_id )
    -- UNIQUE (`node_id`, `sensor_id`)
);

-- drop INDEX on tb_history_data if exists idx_tb_history_data
CREATE INDEX idx_tb_history_data ON tb_history_data ( 
    insert_time,
    node_id 
);

delimiter ;;

drop trigger if exists trg_history_insert_realtime;;
CREATE TRIGGER trg_history_insert_realtime
AFTER INSERT ON tb_history_data
FOR EACH ROW
BEGIN
    declare amount int;
    set amount = 0;
    select count(distinct(node_id)) 
    from tb_realtime_data
    where node_id = new.node_id and sensor_id = new.sensor_id
    into amount;

    if amount = 1 then
        update tb_realtime_data
        set data = new.data,
        insert_time = new.insert_time
        where node_id = new.node_id and sensor_id = new.sensor_id;
    else
        INSERT INTO tb_realtime_data(node_id, sensor_id, data, insert_time) VALUES (
            new.node_id,
            new.sensor_id,
            new.data,
            new.insert_time 
        );
    end if;
END;;

delimiter ;
