delimiter ;;
drop procedure if exists sp_search_full_record;;
create procedure sp_search_full_record()
begin
    select even1.instance_id, bin(even1.sensor_id), even1.data, bin(even2.sensor_id), even2.data from 
    tb_record even1  join tb_record even2 
    on even1.instance_id = even2.instance_id 
    where even1.sensor_id != even2.sensor_id and even1.data != even2.data;
    /*select tb_one.instance_id as instance_id, tb_one.node_id as node_id, tb_one.sensor_set, tb_one.sensor_id as first_sensor, tb_one.data as first_data, tb_two.sensor_id as second_sensor, tb_two.data as second_data, tb_one.sense_time as sense_time from */
    /*(select tb_instance.instance_id as instance_id, tb_instance.node_id as node_id, sensor_set, tb_record.sensor_id as sensor_id, data, sense_time from tb_record left join tb_instance*/
        /*on tb_instance.instance_id = tb_record.instance_id*/
        /*left join tb_sensor_type*/
        /*on tb_record.sensor_id = tb_sensor_type.sensor_id*/
        /*left join tb_node*/
        /*on tb_instance.node_id = tb_node.node_id)  tb_one*/
    /*join tb_one tb_two*/
    /*on tb_one.instance_id = tb_two.instance_id and tb_one.node_id = tb_two.node_id and tb_one.sensor_set = tb_two.sensor_set and tb_one.sense_time = tb_two.sense_time*/
    /*where tb_one.sensor_id != tb_two.sensor_id and tb_one.data != tb_two.data;*/
end;;

delimiter ;


