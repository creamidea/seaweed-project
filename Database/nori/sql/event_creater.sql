delimiter ;;
drop event if exists evn_my_event;;
create event if not exists evn_my_event
on schedule every 10 second
do
    begin
        insert into arnold.temp values('hello','world');
    end;;
delimiter ;
