# -*- encoding: utf-8 -*-
from django import template
from django.template.base import Variable, Library, VariableDoesNotExist

register = Library()

@register.filter(name='cut')
def cut(value, arg):
    return value.replace(arg, '')

@register.tag(name='current_time')
def do_current_time(parser, token):
    try:
        tag_name, format_string = token.split_contents()
    except ValueError:
        msg = '%r tag requires a single argument' % token.split_contents()[0]
        raise template.TemplateSyntaxError(msg)
    return CurrentTimeNode(format_string[1:-1])

# やられたら、やり返す、倍返しだ
import datetime
class CurrentTimeNode(template.Node):
    def __init__(self, format_string):
        self.format_string = str(format_string)

    def render(self, context):
        now = datetime.datetime.now()
        return now.strftime(self.format_string)

class CurrentTimeNode2(template.Node):
    def __init__(self, format_string):
        self.format_string = str(format_string)

    def render(self, context):
        now = datetime.datetime.now()
        context['current_time'] = now.strftime(self.format_string)
        return ''

# register.filter('cut', cut)
