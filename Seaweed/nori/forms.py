#!/usr/bin/env python
# -*- coding: utf-8 -*-

import time
from datetime import datetime, timedelta

from django import forms
from django.forms import ModelForm
# from django.core import validators
# from .models import PondInfo, NodeInfo
from .node_handle import *

class PondInfoForm(ModelForm):
    # def __init__(self, *args, **kwargs):
    #     super(PondInfoForm, self).__init__(*args, **kwargs)
    #     self.fields['pond_name'].label = u'池塘名称'
        
    # class Meta:
    #     model = PondInfo
    #     fields = ['pond_name']
    # pond_id = forms.CharField(max_length = 100)
    # node_id = forms.CharField(max_length = 100)
    pass

class NodeInfoForm(ModelForm):
    # def __init__(self, *args, **kwargs):
    #     super(NodeInfoForm, self).__init__(*args, **kwargs)
    #     self.fields['node_id'].label = u'节点编号'
    # class Meta:
    #     model = NodeInfo
    #     fields = ['node_id']
    pass

class SearchForm(forms.Form):
    now_time = datetime.now().strftime('%Y-%m-%d')
    node_id = forms.IntegerField(label=u'节点编号')
    sensor_id = forms.IntegerField(label=u'传感器编号')
    start_time = forms.DateField(label=u'开始时间', initial=now_time, \
                 widget=forms.TextInput(attrs={'class': 'datepicker'}))
    end_time = forms.DateField(label=u'结束时间', initial=now_time, \
                 widget=forms.TextInput(attrs={'class': 'datepicker'}))
    
    def clean_node_id(self):
        node_id = self.cleaned_data['node_id']
        result = ''
        node_ids = get_all_hd_node_id()
        if node_id in node_ids:
            result = node_id
        else:
            raise forms.ValidationError(u'该节点编号不存在')
        return result

    def clean_sensor_id(self):
        sensor_id = self.cleaned_data['sensor_id']
        result = ''
        sensor_ids = get_all_hd_sensor_id()
        if sensor_id in sensor_ids:
            result = sensor_id
        else:
            raise forms.ValidationError(u'传感器编号不存在')
        return result

    def clean_start_time(self):
        start_time = self.cleaned_data['start_time']
        self.start_time2 = start_time # 用于clean_end_time中的比较
        result = None
        try:
            result = start_time.strftime('%Y-%m-%d')
        except:
            raise forms.ValidationError(u'时间格式不正确')
        finally:
            return result

    def clean_end_time(self):
        end_time = self.cleaned_data['end_time']
        a_day = timedelta(days=1)
        result = None
        if self.start_time2 > end_time:  
            raise forms.ValidationError(u'结束时间需要大于开始时间')
            # result = None
        else:
            end_time = end_time + a_day # 数据库查询的时候需要
            result = end_time.strftime('%Y-%m-%d')
        return result


    
