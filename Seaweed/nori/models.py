from django.db import models

# Create your models here.
# django-admin.py sqlcustom [appname]
class TbHistoryData(models.Model):
    record_id = models.AutoField(primary_key=True, unique=True)
    # record_id = models.IntegerField(primary_key=True, unique=True)
    insert_time = models.DateTimeField()
    node_id = models.IntegerField()
    sensor_id = models.IntegerField()
    data = models.FloatField()
    class Meta:
        db_table = 'tb_history_data'

    def __unicode__(self):
        return u"%d %d %d" % (self.record_id, self.node_id, self.sensor_id)


class TbRealtimeData(models.Model):
    # Django will create column `id` itself. `id` is primary key and auto increment
    node_id = models.IntegerField(primary_key=True)
    sensor_id = models.IntegerField()
    data = models.FloatField()
    insert_time = models.DateTimeField()
    class Meta:
        unique_together=("node_id", "sensor_id")
        db_table = 'tb_realtime_data'

    def __unicode__(self):
        return u"%d %d %f" % (self.node_id, self.sensor_id, self.data)
