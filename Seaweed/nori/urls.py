#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.conf.urls import patterns, include, url
from django.views.generic import TemplateView, RedirectView
# from Seaweed.contact.views import contact as ContactView

from .views import *
from .ajax import *
from .socket_handle import *

urlpatterns = patterns('',
                       url(r'^$', home, name="nori_home"),
                       url(r'^home/$', home, name="nori_home"),
    (r'^node/$', node_view),
    (r'^node/(\d+)/$', node_view),
    (r'^search/$', search_view),
    (r'^sensor/$', sensor_view),
    (r'^sensor/(\d+)/$', sensor_view),
    (r'^control/$', control_view),
    # (r'^([a-zA-Z]+)/(\d+)/$', dispatch),
    (r'^about/$', about_view),
    # (r'^contact/$', ContactView),
)
# Ajax
urlpatterns += patterns('',
                        (r'^ajax/node/test/$', test),
                        (r'^ajax/node/$', ajaxNode),
                        (r'^ajax/node_ids', ajaxNodeIds),
                        (r'^ajax/sensor_ids', ajaxSensorIds),
)

# Test
urlpatterns += patterns('',
                        (r'^control/test/socket', test_socket),
)
