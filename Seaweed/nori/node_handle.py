#!/usr/bin/env python
# -*- encoding: utf-8 -*-
from datetime import timedelta, datetime

from .models import *

# rd: realtim_data
def get_all_rd_node_id():
    '''
    获得所有实时表中的节点编号
    '''
    try:
        return TbRealtimeData.objects.distinct() \
                                     .values_list('node_id', flat=True) \
                                     .order_by('node_id')
    except:
        return []

def get_all_hd_node_id():
    '''
    获得所有历史表中的节点编号
    '''
    try:
        return TbHistoryData.objects.distinct() \
                                    .values_list('node_id', flat=True) \
                                    .order_by('node_id')
    except:
        return []

def get_all_hd_sensor_id():
    try:
        return TbHistoryData.objects.distinct() \
                                    .values_list('sensor_id', flat=True) \
                                    .order_by('sensor_id')
    except:
        return []

def get_history_data(node_id):
    try: 
        return TbHistoryData.objects.order_by('insert_time') \
                                    .filter(node_id=node_id)[:100]
    except:
        return []

def get_all_realtime_data():
    try: 
         all_data = TbRealtimeData.objects.order_by('node_id').all()
    except:
        return []
    else:
        return node_merger(all_data)

def get_all_realtime_data2():
    try: 
        return TbRealtimeData.objects.all().order_by('insert_time')
    except:
        return []

def get_realtime_data_by_node_id(node_id):
    try: 
        return TbRealtimeData.objects.order_by('insert_time') \
                                     .filter(node_id=node_id)
    except:
        return []
    # else:
    #     return node_merger(all_data)

def get_today_all_data():
    try:
        return TbHistoryData.objects.raw(' \
            SELECT record_id, node_id, sensor_id, data, insert_time \
            from `tb_history_data` \
            WHERE DATEDIFF(insert_time, NOW()) = 0 ORDER BY insert_time, sensor_id')
    except:
        return []

def get_today_data_by_node_id(node_id):
    try:
        return TbHistoryData.objects.raw(' \
            SELECT record_id, node_id, sensor_id, data, insert_time \
            from `tb_history_data` \
            WHERE DATEDIFF(insert_time, NOW()) = 0 AND node_id = %s \
            ORDER BY sensor_id, insert_time', [node_id])
    except:
        return []

def get_today_data_by_sensor_id(sensor_id):
    try:
        return TbHistoryData.objects.raw(' \
            SELECT record_id, node_id, sensor_id, data, insert_time \
            from `tb_history_data` \
            WHERE DATEDIFF(insert_time, NOW()) = 0 AND sensor_id = %s \
            ORDER BY sensor_id, insert_time DESC' \
            , [sensor_id])
    except:
        return []


def get_data_bf_a_day(node_id, end):
    try:
        end_time = datetime.strptime(end, "%Y-%m-%d")
        a_day = timedelta(days=1)
        start_time = end_time - a_day
        start = start_time.strftime("%Y-%m-%d")
        end = end_time.strftime("%Y-%m-%d")
        print '$' * 100
        print start
        print end
        return TbHistoryData.objects.raw(' \
            SELECT record_id, node_id, sensor_id, data, insert_time \
            from `tb_history_data` \
            WHERE node_id = %s AND insert_time BETWEEN %s AND  %s \
            ORDER BY sensor_id, insert_time DESC'\
            , [node_id, start, end])[0:]
    except Exception, e:
        print e
        return []

def get_records(node_id, start, end):
    try:
        return TbHistoryData.objects.raw(\
                    'SELECT record_id, node_id, sensor_id, data, insert_time \
                    from `tb_history_data` WHERE node_id = %s \
                    ORDER BY sensor_id, insert_time', \
                    [node_id])[int(start):int(end)]
    except:
        return[]


def search_history(o):
    node_id = o['node_id']
    sensor_id = o['sensor_id']
    start_time = o['start_time']
    end_time = o['end_time']
    records = []
    try:
        # records = TbHistoryData.objects.raw(\
        #     'SELECT record_id, node_id, sensor_id, data, insert_time \
        #     FROM `tb_history_data` WHERE node_id = %s AND sensor_id = %s \
        #     AND insert_time >= %s AND insert_time <= %s \
        #     ORDER BY insert_time DESC', \
        #     [node_id, sensor_id, start_time, end_time])[0:]
        records = TbHistoryData.objects.filter(node_id=node_id,\
                                               sensor_id=sensor_id, \
                                               insert_time__gte=start_time,\
                                               insert_time__lte=end_time)\
                                       .order_by('insert_time')
    except Exception, e:
        print e
    return records

# ////////////////////////////////////////////
def node_merger(all_data):
    """
    将数据库查处的数据，以一个节点为一条的数据合并
    """
    nodes = []                  # 用于存放最终的结果
    records = []                # 用于每一个节点临时存放一条记录
    pre_node_id = all_data[0].node_id
    # print pre_node_id
    for data in all_data:
        # 如果当前的不等于上一个，则说明是另一个节点，应将数据保存。
        if data.node_id != pre_node_id:
            nodes.append(records)
            records = []        # 清除记录表
        records.append(data)
        pre_node_id = data.node_id
    nodes.append(records)
    return nodes
