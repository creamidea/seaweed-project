#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django import forms
from django.utils.translation import ugettext, ugettext_lazy as _
from django.contrib.auth.forms import UserCreationForm, PasswordChangeForm
from django.contrib.auth.hashers import (
    MAXIMUM_PASSWORD_LENGTH, UNUSABLE_PASSWORD, identify_hasher,
    )
from .models import MyUser as User

class UserRegisterForm(UserCreationForm):
    
    error_messages = {
        'duplicate_username': u"用户名已被注册.",
        'password_mismatch': u"密码不匹配",
    }
    
    def __init__(self, *args, **kwargs):
        "重命名各个的label"
        super(UserCreationForm, self).__init__(*args, **kwargs)
        self.fields['username'].label = u''
        self.fields['username'].help_text = u''
        # self.fields['username'].help_text = u'要求小于30个字符，可以包含字母，数字以及@/./+/-/_'
        self.fields['username'].widget = forms.TextInput(attrs={'class': 'form-control', 'placeholder': u'登录名 小于30字符，可包含@/./+/-/_', 'required': True})
        
        self.fields['password1'].label = u''
        self.fields['password1'].widget = forms.TextInput(attrs={'type':'password', 'class': 'form-control', 'placeholder': u'密码', 'required': True})
        
        self.fields['password2'].label = u''
        self.fields['password2'].widget = forms.TextInput(attrs={'type':'password', 'class': 'form-control', 'placeholder': u'确认密码', 'required': True})
        self.fields['password2'].help_text = u''
        
    # phone = forms.RegexField(label=u"手机号码", max_length=11,
    #                             regex=r'^[\d]+$',
    #                             help_text=u"输入11位的手机号码",
    #                             error_messages={
    #                                 'invalid': u"只能输入11位的手机号码",})

    # class Meta:
    #     model = User
    #     fileds = ("phone",)

    # def clean_phone(self):
    #     # Since User.username is unique, this check is redundant,
    #     # but it sets a nicer error message than the ORM. See #13147.
    #     phone = self.cleaned_data["phone"]
    #     try:
    #         User._default_manager.get(phone=phone)
    #     except User.DoesNotExist:
    #         return phone
    #     raise forms.ValidationError(self.error_messages[u'输入的手机号码无效'])

    # def save(self, commit=True):
    #     user = super(UserRegisterForm, self).save(commit=False)
    #     user.set_phone(self.cleaned_data["phone"])
    #     if commit:
    #         user.save()
    #     return user

class MyPasswordChangeForm(PasswordChangeForm):
    """
    我自己的改变密码是设置
    """
    error_messages =  {
        'password_incorrect': u'你输入的原密码错误'
    }
        
    # old_password = forms.CharField(
    #     label= u"旧密码",
    #     widget=forms.PasswordInput,
    #     max_length=MAXIMUM_PASSWORD_LENGTH,
    # )
    def __init__(self, *args, **kwargs):
        super(PasswordChangeForm, self).__init__(*args, **kwargs)
        self.fields['old_password'].label = u'旧密码'
        self.fields['old_password'].widget = forms.TextInput(attrs={'class': 'form-control', 'placeholder': u'输入旧密码', 'required': True})
        self.fields['new_password1'].widget = forms.TextInput(attrs={'class': 'form-control', 'placeholder': u'输入新密码', 'required': True})
        self.fields['new_password2'].widget = forms.TextInput(attrs={'class': 'form-control', 'placeholder': u'再次输入新密码', 'required': True})

