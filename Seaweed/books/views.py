# Create your views here.
from django.shortcuts import render_to_response
from django.http import HttpResponse
# from django.views.generic import list_detail # deprecated
from django.views.generic.list import ListView

from .models import Book, Publisher, Author

# def search_form(request):
#     return render_to_response('search_form.html')

def search(request):
    errors = []
    if 'q' in request.GET:
        q = request.GET['q']
        if not q:
            errors.append('Enter a search term')
        elif len(q) > 20:
            errors.append('Please enter at least 20 characters.')
        else:
            books = Book.objects.filter(title__icontains=q)
            return render_to_response('search_results.html', 
                                      {'books': books, 'query': q})
    return render_to_response('search_form.html', {'errors': errors})

class PublisherListView(ListView):

    model = Publisher

    def get_queryset(self):
        return Publisher.objects.all()


class AuthorListView(ListView):
    model = Author
    def get_queryset(self):
        return Author.objects.all()

class BookListView(ListView):
    model = Book
    def header(self, *args, **kwargs):
        last_book = self.get_queryset().latest('publication_date')
        response = HttpResponse('')
        # RFC 1123 date format
        response['Last-Modified'] = last_book.publication_date.strftime('%a, %d %b %Y %H: %S')
        response['Server'] = 'creamidea v1.0'
        return response
        
    def get_context_data(self, **kwargs):
        context = super(BookListView, self).get_context_data(**kwargs)
        import random
        context['number'] = random.randrange(1, 100)
        return context
