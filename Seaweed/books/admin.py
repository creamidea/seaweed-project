# -*- encoding: utf-8 -*-
from django.contrib import admin
from Seaweed.books.models import Publisher, Author, Book

class AuthorAdmin(admin.ModelAdmin):
    list_display = ('first_name', 'last_name', 'email')
    search_fields = ('first_name', 'last_name')

class BookAdmin(admin.ModelAdmin):
    list_display = ('title', 'publisher', 'publication_date')
    list_filter = ('publication_date',)
    date_hierarchy = 'publication_date'
    ordering = ('-publication_date',)
    # 编辑页面包含
    # fields = ('title', 'authors', 'publisher',)
    # authors变成有两个
    filter_horizontal = ('authors',)
    # publisher旁边有一个放大镜，这里填写ID 
    raw_id_fields = ('publisher', )

admin.site.register(Publisher)
admin.site.register(Author, AuthorAdmin)
admin.site.register(Book, BookAdmin)

