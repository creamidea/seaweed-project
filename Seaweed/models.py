#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.db import models
from django.contrib.auth.models import User

class MyUser(User):
    phone=models.CharField(max_length=11)
    
    def get_phone(self):
        return self.phone

    def set_phone(self, phone_number):
        self.phone = phone_number
