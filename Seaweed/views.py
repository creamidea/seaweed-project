#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.shortcuts import render_to_response
from django.http import HttpResponseRedirect, HttpResponse
from django.template import RequestContext
from django.contrib.auth.decorators import login_required
from django.contrib.auth.views import login, password_change, password_change_done

from .forms import UserRegisterForm, MyPasswordChangeForm

# from .nori.forms import PondInfoForm, NodeInfoForm

def site_info(request):
    # return {
    #     'title': '水产项目',
    #     'project_name': '水产项目',
    #     # 'contents': ["主页", "节点", "传感器", "关于", "联系"],
    # }
    return {
        'title': '紫菜项目',
        'project_name': '紫菜项目',
        'contents': (
            ("home", "主页"), 
            ("node", "节点"), 
            ("search", "查询"), 
            # ("sensor", "传感器"), 
            # ("control", "控制"),
            ("about", "关于"), 
            ("contact", "联系"),
        ),
    }

def home(request):
    # 主页
    user = request.user
    if not user.is_authenticated():
        html = '''
        <h1>welcome here. here is test!</h1>
        <a href="/accounts/login">login</a>
        <a href="/accounts/register">register</a>
        '''
    else:                       # login
        html = '''
        <h1>welcome here. %s</h1>
        <p>You Join Date: %s</p> 
        <p>You Last Login: %s</p>
        <a href="/accounts/profile">profile</a>
        <a href="/accounts/logout">logout</a>
        ''' % (user.username, user.date_joined, user.last_login)
        
    return render_to_response('index.html',
                              context_instance=RequestContext(request,processors=[site_info]))
    # return HttpResponse(html)

def my_login(request):
    #: Attention: request.POST中checkout被勾选则会有
    #: {'remember': 'on'}
    if request.POST:
        check = request.POST.get('remember', None)
        if check == 'on':
            # 2 weeks = 1209600 seconds
            request.session.set_expiry(1209600)
        else:
            request.session.set_expiry(0)
    rlt =  login(request)
    request.PORT = None
    return rlt
    
def register(request):
    # 用户注册的界面
    if request.method == 'POST':
        form = UserRegisterForm(request.POST)
        if form.is_valid():
            new_user = form.save()
            return HttpResponseRedirect('/accounts/profile/')
    else:
        form = UserRegisterForm()
    return render_to_response('registration/register.html', 
                              {
                                  'form': form,
                              }, 
                              context_instance=RequestContext(request))

@login_required
def my_password_change(request):
    return password_change(request, password_change_form=MyPasswordChangeForm)

@login_required
def user_profile_view(request):
    # 账户设置
    # 用户profile界面
    return render_to_response('registration/profile.html',
                              context_instance = RequestContext(request))

# @login_required
# def change_password_view(request):
#     from django.contrib.auth.views import password_change
#     url = '/accounts/password/change/done/'
#     defaults = {
#         'post_change_redirect': url

#     }
#     defaults['template_name'] = 'password_change_form.html'
#     return password_change(request, **defaults)

# @login_required
# def change_password_done_view(request):
#     from django.contrib.auth.views import password_change_done

    
def create_playlist(request):
    from django.contrib import messages
    messages.add_message(request, messages.INFO, "Your playlist was added successfully!")
    return render_to_response('playlists/create.html', 
                              context_instance=RequestContext(request))

# from django.contrib.auth.models import User, Group, Permission
# from django.contrib.contenttypes.models import ContentType

# content_type = ContentType.objects.get(app_label='myapp', model='BlogPost')
# permission = Permission.objects.create(codename='can_publish',
#                                        name='Can Publish Posts',
#                                        content_type=content_type)
# user = User.objects.get(username='duke_nukem')
# group = Group.objects.get(name='wizard')
# group.permissions.add(permission)
# user.groups.add(group)

def test(request):
    return HttpResponse('Here is test')
