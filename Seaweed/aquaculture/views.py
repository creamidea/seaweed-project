# Create your views here.
# -*- coding: utf-8 -*-

import json

from django.shortcuts import render_to_response
from django.http import HttpResponseRedirect, HttpResponse, Http404
from django.template import RequestContext
from django.contrib.auth.decorators import login_required

from .forms import ProductRegister
from .dboperator.db_operator import DbOperator
from .ajax import pond_action, node_action

db = DbOperator()

@login_required
def pond_view(request, pond_name):
    # 池塘的视图
    # 池塘的基本信息
    # 池塘的采集数据值
    pond_name = pond_name
    user_id = request.user.id
    print db.is_pond_exist(user_id, pond_name)
    if db.is_pond_exist(user_id, pond_name):
        # ponds = db.db_s_user_all_pond(user_id)
        realtime_data = db.db_s_certain_pond_all_realtime_data(user_id, pond_name)
        today_data = db.db_s_certain_pond_today_data(user_id, pond_name)
        yesterday_data = db.yesterday_data(user_id, pond_name)
        (pump_id, pump_name) = db.db_s_certain_pond_pump(user_id, pond_name)
        pump_status = db.certain_pump_state(user_id, pump_id)
        pump = (pump_id, pump_name, pump_status)
        # print '$#E' * 10
        # print realtime_data
        # print today_data
        return render_to_response('pond.html', 
                              {
                                  'pond_name': pond_name,
                                  'pump': pump,
                                  'realtime_data': realtime_data,
                                  'today_data': today_data,
                                  'yesterday_data': yesterday_data,
                              },
                              context_instance=RequestContext(request))
    else:
        raise Http404()

@login_required
def pond_settings_view(request):
    user_id = request.user.id
    ponds = db.db_s_user_all_pond(user_id)
    # print '///' * 30
    # print '@#_' * 30
    # print ponds
    if request.method == 'POST':
        form = ProductRegister(request.POST)
        if form.is_valid():
            result = form.save(user_id)
            return HttpResponse(result)
    else:
        form = ProductRegister()
    return render_to_response('pond-settings.html', {
        'form': form,
        'ponds': ponds,
    },context_instance = RequestContext(request))

@login_required
def pond_handle_view(request):
    # :param request: request object
    # :rtype: {status: 200/500, error: 'error'}
    user_id = request.user.id
    # print '///' * 30
    # print user_id
    mesg = request.POST['mesg']
    # print mesg
    mesg = json.loads(mesg)
    action = mesg.get('action', 'default')
    data = mesg.get('data', None)
    # print action
    mesg = pond_action[action](user_id, data)
    return HttpResponse(mesg)

@login_required
def node_handle_view(request):
    # :param request: request object
    # :rtype: {status: 200/500, error: 'error'}
    user_id = request.user.id
    print '///' * 30
    print user_id
    mesg = request.POST['mesg']
    print mesg
    mesg = json.loads(mesg)
    action = mesg.get('action', 'default')
    data = mesg.get('data', None)
    print action, data
    mesg = node_action[action](user_id, data)
    return HttpResponse(mesg)

@login_required
def latest_view(request):
    """
    更新器
    返回值：
    pump_status
    
    """
    user_id = request.user.id
    try:
        data = request.POST['data']
        data = json.loads(data)
        p_ids = data.get('pump_id', None)
        n_ids = data.get('node_id', None)
        r_data = {
            'pump_id': {},
            'node_id': {},
            'error': 1,
        }
        for id in p_ids:
            r_data['pump_id'][id] = db.certain_pump_state(user_id, id)
        for id in n_ids:
            r_data['node_id'][id] = db.latest_node_data(user_id, id)
    except :
        r_data['error'] = 1
    else:
        r_data['error'] = 0
    mesg = json.dumps(r_data)
    print mesg
    return HttpResponse(mesg)
    # return HttpResponse(1)

@login_required
def history_view(request):
    print('//////////////////////////////////')
    print(request.POST)
    user_id = request.user.id
    time = request.POST['time']
    node_id = request.POST['node_id']
    pond_name = request.POST['pond_name']
    result = db.history_data(user_id, pond_name, node_id, time)
    mesg = json.dumps(result)
    return HttpResponse(mesg)
