# -*- encoding: utf-8 -*-
from django import template
from django.template.base import Variable, Library, VariableDoesNotExist

from Seaweed.nori.head.py import d_sensor_name

register = Library()

# pump_status = {
#     "0": u'开',
#     "1": u'断',
#     "2": u'欠压',
# }

# node_state = {
#     '1': u'正常',
#     '0': u'欠压', 
#     '-1':u'异常',
# }
    
@register.filter(name='id_to_name')
def id_to_name(value, arg):
    return d_sensor_name[value]

@register.filter(name='status_to_cn')
def status_to_cn(value, arg):
    value = str(value)
    # return pump_status[value]
    return u'测试'
