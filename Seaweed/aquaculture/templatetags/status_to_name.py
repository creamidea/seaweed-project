# -*- encoding: utf-8 -*-
from django import template
# from django.template.base import Variable, Library, VariableDoesNotExist

register = template.Library()

pump_status = {
    "0": u'开启',
    "1": u'断开',
    "2": u'欠压',
}

pump_css_status = {
    "0": "btn btn-success",
    "1": "btn btn-danger",
    "2": "btn btn-warning",
}

node_state = {
    '1': u'正常',
    '0': u'欠压', 
    '-1':u'异常',
}

@register.filter(name='status')
def status(value, arg):
    value = str(value)
    return pump_status[value]

@register.filter(name='css_status')
def css_status(value, arg):
    value = str(value)
    return pump_css_status[value]
