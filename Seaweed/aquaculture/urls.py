from django.conf.urls import patterns, include, url
from .views import pond_view, pond_settings_view, pond_handle_view, node_handle_view, latest_view, history_view
from .ajax import add_pond, registe_node, update_pond, delete_pond, update_node, delete_node

urlpatterns = patterns('',
                       # url(r'^$',
                       #     pond_view, name="pond"),
    url(r'^settings/$',
        pond_settings_view, name="pond_setting"),
    url(r'^pond/(.+)/$', pond_view, name="pond_view")

                       # url(r'^$', home, name="nori_home"),
                       # url(r'^home/$', home, name="nori_home"),
                       # (r'^node/$', node_view),
                       # (r'^node/(\d+)/$', node_view),
                       # (r'^search/$', search_view),
                       # (r'^sensor/$', sensor_view),
                       # (r'^sensor/(\d+)/$', sensor_view),
                       # (r'^control/$', control_view),
                       # # (r'^([a-zA-Z]+)/(\d+)/$', dispatch),
                       # (r'^about/$', about_view),
                       # # (r'^contact/$', ContactView),
)

urlpatterns += patterns('',
                        (r'^ajax/pond/$', pond_handle_view),
                        (r'^ajax/node/$', node_handle_view),
                        (r'^ajax/latest/$', latest_view),
                        (r'^ajax/history/$', history_view),
                        
                        # (r'^ajax/add/pond/$', add_pond),
                        # (r'^ajax/delete/pond/$', delete_pond),
                        # (r'^ajax/update/pond/$', update_pond),
                        # (r'^ajax/registe/node/$', registe_node),
                        # (r'^ajax/update/node/$', update_node),
                        # (r'^ajax/delete/node/$', delete_node),
)
