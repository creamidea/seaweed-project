#!/usr/bin/env python
# -*- coding: utf-8 -*-

from datetime import date
from django.db import connection, transaction

class DbOperator():

    # 返回该用户池塘的信息：池塘ID和池塘的名称
    def db_s_user_all_pond(self, user_id):
        cur = connection.cursor()
        try:
            user_id = int(user_id)
            # cur.execute("select pond_id, pond_name from tb_pond where user_id = %s", [user_id])
            cur.execute('''
            SELECT pond_id, pond_name, node_id
            FROM tb_pond LEFT JOIN tb_node USING(pond_id, user_id)
            WHERE user_id = %s
            ORDER BY pond_id''',
            [user_id])
            # result = cur.fetchall()
            # result = self.__merge_pond(result)
        except Exception, e:
            print e
            result = {}
        finally:
            if cur:
                cur.close()
            print '//' * 30
            result = (
                ('1', 'pond1', (('101', 'node1'), ('102', 'node2'), ('103', 'node3'))),
                ('2', 'pond2', (('104', 'node1'), ('105', 'node2'), ('106', 'node3'))),
                      )
            return result
    def is_pond_exist(self, user_id, pond_name):
        """
        确定池塘是否存在
        """
        cur = connection.cursor()
        # print '----' * 20
        # print user_id, pond_name
        try:
            user_id = int(user_id)
            cur.execute('''
            select count(pond_name)
            from tb_pond
            where user_id = %s and pond_name = %s
            ''', [user_id, pond_name])
            num = cur.fetchone()[0]
            print num
            if num == 1:
                return True
            else:
                return False
        except Exception, e:
            print e
            return False
    
    # 检查节点ID是否为有限的节点ID
    # 有效为1
    # 无效为0
    def db_s_check_node_legal(self, node_id):
        # with connection.cursor() as cur:
        try:
            cur = connection.cursor()
            node_id = int(node_id)
            cur.execute('''
            SELECT count(node_id)
            FROM tb_node_producted
            WHERE node_id = '%s' '''
            , [node_id])
            num = cur.fetchone()
            # print '$' * 100
            if num[0] == 0:
                return 0
            else:
                return 1
        except:
            return 0
        finally:
            if cur:
                cur.close()


    # 确定池塘和节点的关系
    def db_s_certain_pond_node(self, user_id, pond_name):
        cur = connection.cursor()
        try:
            cur.execute('''
            SELECT node_id, node_name, node_state FROM tb_node
            LEFT JOIN tb_pond USING (user_id)
            WHERE user_id = %s AND pond_name = %s'''
            , [user_id, pond_name])
            result = cur.fetchone()
        except:
            result = []
        finally:
            if cur:
                cur.close()
            return result

    # 确定节点的状态
    def db_s_certain_node_state(self, user_id, node_name):
        cur = connection.cursor()
        try:
            user_id = int(user_id)
            cur.execute("select node_state from tb_node where user_id = '%s' and node_name = '%s'", [user_id, node_name])
            result = cur.fetchall()
        except :
            result = []
        finally:
            if cur:
                cur.close()
            return result

    # 确定水泵的状态
    # 为何水泵表里面不存储user_id?
    def certain_pump_state(self, user_id, pump_id):
        cur = connection.cursor()
        try:
            user_id = int(user_id)
            cur.execute(" \
                select pump_state from tb_user, tb_eq, tb_pump \
                where tb_user.user_id = %s \
                and tb_eq.eq_id = tb_pump.eq_id \
                and tb_pump.pump_id = %s ", [user_id, pump_id])
            result = cur.fetchall()
        except :
            result = []
        finally:
            if cur:
                cur.close()
            result = 1
            return result


    # 确定池塘和水泵
    @transaction.commit_on_success
    def db_s_certain_pond_pump(self, user_id, pond_name):
        cur = connection.cursor()
        try:
            result = cur.callproc('s_certain_pond_pump',[user_id, pond_name])
        except :
            result = []
        finally:
            if cur:
                cur.close()
            result = (1, '宇宙牌水泵')
            return result

    # 取出池塘所有的实时信息
    @transaction.commit_on_success
    def db_s_certain_pond_all_realtime_data(self, user_id, pond_name):
        cur = connection.cursor()
        try:
            result = cur.callproc('s_certain_pond_all_realtime_data',[user_id, pond_name])
        except :
            result = []
        finally:
            if cur:
                cur.close()
            result = []
            record1 = ('1', '2013-11-08 15:02', (('101', u'光照', 100), ('102', u'湿度', 300)))
            record2 = ('2', '2013-11-08 15:02', (('101', u'光照', 300), ('102', u'湿度', 500)))
            result.append(record1)
            result.append(record2)
            return result
        
    # 取出池塘今天的数据
    @transaction.commit_on_success
    def db_s_certain_pond_today_data(self, user_id, pond_name):
        import datetime
        from random import random
        cur = connection.cursor()
        try:
            start_time = date.today().strftime('%Y%m%d') + '000000'
            end_time = datetime.now().strftime('%Y%m%d%H%M%S')
            print "start_time = %s, end_time = %s" %(start_time,end_time)
            result = cur.callproc('s_certain_pond_time_range_data', [user_id, pond_name,start_time, end_time])
        except :
            result = []
        finally:
            if cur:
                cur.close()
            result = []
            start = datetime.datetime.now()
            internal = datetime.timedelta(minutes = 5)
            sensors = []
            for i in range(1, 288):
                oxy = float(random() * 100)
                temp = float(random() * 40)
                time = (start + internal * i).strftime('%Y-%m-%d %H:%M')
                sensor = (time, oxy, temp)
                sensors.append(sensor)
            record1 = ('1', ('101', u'溶氧度'), ('102', u'温度'), sensors)
            # record1 = ('1', ('101', u'溶氧度'), ('102', u'温度'), (('2013-11-09 18:52', 200, 400), ('2013-11-09 20:53', 10, 500)))
            record2 = ('2', ('101', u'溶氧度'), ('102', u'温度'), (('2013-11-09 20:52', 100, 400), ('2013-11-09 20:53', 10, 500)))
            result.append(record1)
            result.append(record2)
            return result

    @transaction.commit_on_success
    def yesterday_data(self, user_id, pond_name):
        result = []
        record1 = ('1', ('101', u'溶氧度'), ('102', u'温度'), (('2013-11-09 20:52', 100, 400), ('2013-11-09 20:53', 10, 500)))
        record2 = ('2', ('101', u'溶氧度'), ('102', u'温度'), (('2013-11-09 20:52', 100, 400), ('2013-11-09 20:53', 10, 500)))
        result.append(record1)
        result.append(record2)
        return result

    def history_data(self, user_id, pond_name, node_id, time):
        # 给定的时间格式为
        # yesterday, week, month,
        # start_time:end_time
        record2 = ((('101', u'溶氧度'), ('102', u'温度')), (('2013-11-09 20:52', 100, 400), ('2013-11-09 20:53', 10, 500)))
        return record2
        
    # 给定时间范围查找池塘的采集数据
    @transaction.commit_on_success
    def db_s_certain_pond_time_range_data(self, user_id, pond_name, start_time, end_time):
        cur = connection.cursor()
        try:
            print "start_time = %s, end_time = %s" %(start_time,end_time)
            result = proc_cur.callproc('s_certain_pond_time_range_data', [user_id, pond_name,start_time, end_time])
        except :
            result = []
        finally:
            if cur:
                cur.close()
            return result

    @transaction.commit_on_success
    def db_i_registe(self, user_id, pond_name, node_id):
        """
        注册：将池塘名称和节点ID绑定
        成功返回1
        失败返回0
        """
        if user_id and pond_name and node_id:
            proc_cur = connection.cursor()
            try:
                user_id = int(user_id)
                node_id = int(node_id)
                proc_cur.callproc('i_registe', [user_id, pond_name, node_id])
            except Exception, e:
                print e
                result = str(e)
            else:
                result = 0
            finally:
                if proc_cur:
                    proc_cur.close()
                return result
        else:
            return 'param error'

    def latest_node_data(self, user_id, node_id):
        return ('2013-11-15 09:32', ('101', u'溶氧度', 100), ('102', u'温度',200))
        
    @transaction.commit_on_success
    def insert_pond(self, user_id, pond_name):
        """
        插入池塘的名称，成功返回0，失败返回异常信息
        """
        if user_id and pond_name:
            cur = connection.cursor()
            try:
                user_id = int(user_id)
                cur.execute(
                    '''
                insert into tb_pond(user_id, pond_name) values(%s, %s)
                '''
                , [user_id, pond_name])
            except Exception, e:
                result = e
            else:
                result = 0
            finally:
                if cur:
                    cur.close()
                return result
        else:
            return 'user_id or pond_name is none'

    @transaction.commit_on_success
    def delete_pond(self, user_id, pond_name):
        """
        删除池塘
        """
        if user_id and pond_name:
            cur = connection.cursor()
            try:
                user_id = int(user_id)
                cur.execute('''
                delete from tb_pond
                where user_id = %s and pond_name = %s
                ''', [user_id, pond_name])
            except Exception, e:
                result = e
            else:
                result = 0
            finally:            
                if cur:
                    cur.close()
                return result
        else:
            return 'param error'

    @transaction.commit_on_success
    def db_u_set_pond_name(self, user_id, orign_name, new_name):
        """
        设置池塘的名称
        成功为1
        失败为0
        """
        if user_id and orign_name and new_name:
            cur = connection.cursor()
            try:
                user_id = int(user_id)
                cur.execute('''
                UPDATE tb_pond SET pond_name = %s
                WHERE user_id = %s and pond_name = %s'''
                            , [new_name, user_id, orign_name])
                result = cur.fetchall()
            except Exception, e:
                result = e
            else:
                result = 0
            finally:
                if cur:
                    cur.close()
                return result
        else:
            return 'param error'

    @transaction.commit_on_success
    def delete_node(self, user_id, node_id):
        """
        删除节点
        """
        if user_id and node_id:
            # print '##' * 20
            # print user_id, node_id
            cur = connection.cursor()
            try:
                user_id = int(user_id)
                cur.execute('''
                delete from tb_node
                where user_id = %s and node_id = %s
                ''', [user_id, node_id])
            except Exception, e:
                result = e
            else:
                result = 0
            finally:            
                if cur:
                    cur.close()
                return result
        else:
            return 'param error'

    # DbOperator私有方法
    def __merge_pond(self, result):
        """
        将数据库中查出的池塘信息
        以池塘ID为键，以其他数据为元组形式存放的值
        因为result中的值已经以pond_id升序形式排列
        所以当pond_id改变时新建键
        输入：((42L, u'test', 3L), (42L, u'test', 2L), (42L, u'test', 1L), (43L, u'test2', 4L))
        输出：{'42': [(u'test', 3L), (u'test', 2L), (u'test', 1L)], '43': [(u'test2', 4L)]}
        """
        p = {}
        for r in result:
            key = str(r[0])
            try:
                p[key]
            except KeyError:    # 键不存在，说明是新值
                p[key] = []
            finally:
                p[key].append((r[1], r[2]))
        return p
