# -*- coding: utf-8 -*-
from django import forms
from django.utils.translation import ugettext, ugettext_lazy as _

from .dboperator.db_operator import DbOperator

db_operator = DbOperator()
class ProductRegister(forms.Form):
    """
    产品注册表，将池塘名称和节点的编号绑定在一起
    """
    pond_name = forms.CharField(max_length=20, label=u"池塘名称")
    node_id = forms.CharField(max_length=20, label=u"节点编号")

    class Meta:
        fileds = ("pond_name", "node_id")

    def clean_node_id(self):
        node_id = self.cleaned_data['node_id']
        # flag = db_operator.db_s_check_node_legal(node_id)
        # print '#'*100
        # print flag
        flag = db_operator.db_s_check_node_legal(node_id)
        if flag is 1:
            return node_id
        else:
            raise forms.ValidationError(u'输入的节点编号无效')

    def save(self, user_id=None):
        pond_name = self.cleaned_data['pond_name']
        node_id = self.cleaned_data['node_id']
        # return "User_id: %s, pond_name: %s, node_id: %s" % (user_id, pond_name, node_id)
        flag = db_operator.db_i_registe(user_id, pond_name, node_id)
        return flag
