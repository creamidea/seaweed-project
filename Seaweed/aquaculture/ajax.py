# -*- coding: utf-8 -*-
import json

from django.http import HttpResponseRedirect, HttpResponse
from django.template import RequestContext
from django.contrib.auth.decorators import login_required

from .dboperator.db_operator import DbOperator
db = DbOperator()

# 消息封装和解析的方法
class Data(object):
    """
    数据的封装与解析
    """
    def __init__(self, status, error="unknown"):
        """
        """
        self.status = status
        self.error = error
        
    def stringify(self):
        """
        封装
        """
        status = self.status
        error = self.error
        r = ""
        try:
            r = json.dumps({
                'status': status,
                'error': error,
            })
        except Exception, e:
            print e
            r = json.dumps({
                'status': 500,
                'error': e,
            })
        finally:
            return r
    def parse(self, data):
        # """
        # 解析
        # """
        # try:
        #     json.loads(data)
        # except Exception, e:
        #     print e
        pass

def add_pond(user_id, data):
    """
    增加池塘
    """
    print 'Here is add pond ajax handle'
    pond_name = data.get('pond_name', None)
    error = 'unknow'
    if pond_name:
        error = db.insert_pond(user_id, pond_name)
    if error is 0:
        status = 200
        error = data
    else:
        status = 500
    data = Data(status, error)
    return data.stringify()

def delete_pond(user_id, data):
    """
    删除池塘
    """
    error = 'unknow'
    pond_name = data.get('pond_name', None)
    if pond_name:
        error = db.delete_pond(user_id, pond_name)
    if error is 0:
        status = 200
        error = data
    else:
        status = 500
    data = Data(status, error)
    return data.stringify()

def update_pond(user_id, data):
    """
    修改池塘的名称
    """
    error = 'unknow'
    new_pond_name = data.get('new_name', None)
    old_pond_name = data.get('old_name', None)
    if new_pond_name and old_pond_name:
        error = db.db_u_set_pond_name(user_id, old_pond_name, new_pond_name)
    if error is 0:
        status = 200
        error = data
    else:
        status = 500
    data = Data(status, error)
    return data.stringify()

def registe_node(user_id, data):
    """
    注册节点
    """
    error = 'unknow'
    node_id = data.get('node_id', None)
    pond_name = data.get('pond_name', None)
    if node_id and pond_name:
        error = db.db_i_registe(user_id, pond_name, node_id)
    if error is 0:
        status = 200
        error = data
    else:
        status = 500
    data = Data(status, error)
    return data.stringify()

def update_node(user_id, data):
    """
    更新池塘的节点
    """
    user_id = request.user.id
    try:
        pass
    except :
        pass
    finally:
        return HttpResponse('0')

def delete_node(user_id, data):
    error = 'unknow'
    node_id = data.get('node_id', None)
    if node_id:
        error = db.delete_node(user_id, node_id)
    if error is 0:
        status = 200
        error = data
    else:
        status = 500
    data = Data(status, error)
    return data.stringify()

def default(user_id, data):
    error = '无此操作'
    data = Data(500, error)
    return data.stringify()

# 定义执行字典
pond_action = {
    'add': add_pond,
    'remove': delete_pond,
    'update': update_pond,
    'default': default,
}

node_action = {
    'add': registe_node,
    'remove': delete_node,
    'update': update_node,
    'default': default,
}
