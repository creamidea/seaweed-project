module.exports = function(grunt) {

    // Project configuration.
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        uglify: {
            options: {
                banner: '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> */\n'
            },
            build: {
                src: 'src/<%= pkg.name %>.js',
                dest: 'build/<%= pkg.name %>.min.js'
            }
        },
        less: {
            development: {
                options: {
                    paths: ["css/"],
                    yuicompress: true
                },
                files: {
                    "nori/css/nori.css": "nori/less/nori.less",
                    "aquaculture/css/style.css": "aquaculture/less/style.less"
                }
            },
            production: {
                options: {
                    paths: ["css/"],
                    yuicompress: true
                },
                files: {
                    "nori/css/nori.css": "nori/less/nori.less"
                }
            }
        },
        coffee: {
            // compile: {
            //     files: {
            //         'js/helper.js': 'coffee/helper.coffee', // 1:1 compile
            //         'js/nori-view.js': 'coffee/nori-view.coffee',
            //         'js/nori-search.js': 'coffee/nori-search.coffee'
            //         // 'path/to/another.js': ['path/to/sources/*.coffee', 'path/to/more/*.coffee'] // compile and concat into single file
            //     }
            // },

            // compileBare: {
            //     options: {
            //         bare: true
            //     },
            //     files: {
            //         'path/to/result.js': 'path/to/source.coffee', // 1:1 compile
            //         'path/to/another.js': ['path/to/sources/*.coffee', 'path/to/more/*.coffee'] // compile and concat into single file
            //     }
            // },

            // compileJoined: {
            //     options: {
            //         join: true
            //     },
            //     files: {
            //         'path/to/result.js': 'path/to/source.coffee', // 1:1 compile, identical output to join = false
            //         'path/to/another.js': ['path/to/sources/*.coffee', 'path/to/more/*.coffee'] // concat then compile into single file
            //     }
            // },

            // compileWithMaps: {
            //     options: {
            //         sourceMap: true
            //     },
            //     files: {
            //         'path/to/result.js': 'path/to/source.coffee', // 1:1 compile
            //         'path/to/another.js': ['path/to/sources/*.coffee', 'path/to/more/*.coffee'] // concat then compile into single file
            //     }
            // },

            glob_to_multiple: {
                expand: true,
                flatten: true,
                // cwd: 'path/to',
                src: ['coffee/*.coffee'],
                dest: 'js',
                ext: '.js'
            }
        },
        watch: {
            // coffee: {
            //     files: 'coffee/*.coffee',
            //     task: ['coffee'],
            //     options: {
            //         events: ['all'],
            //         nospawn: true
            //         // livereload: true
            //     }
            // },
            less: {
                files: ["nori/less/*.less", "aquaculture/less/*.less"],
                tasks: ['less'],
                options: {
                    events: ['all'],
                    nospawn: true
                }
            }
        }
    });
    // Load grunt watch
    grunt.loadNpmTasks('grunt-contrib-watch');
    // Load the plugin that provides the "uglify" task.
    grunt.loadNpmTasks('grunt-contrib-uglify');
    // Less compile
    grunt.loadNpmTasks('grunt-contrib-less');
    // coffee compile
    grunt.loadNpmTasks('grunt-contrib-coffee');

    // Default task(s).
    grunt.registerTask('default', ['less']);
    // grunt.registerTask('default', ['uglify', 'watch']);

};
