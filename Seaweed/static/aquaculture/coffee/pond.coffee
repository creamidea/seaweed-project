$ ->
  # alert ' here is pond '
  # 本页全局变量
  $pump_status = $('#pump_status')
  pump_id = $pump_status.attr("pump_id")
  update_interval = 300000        #5min
  status =
    '0': ['btn btn-success', '开启']
    '1': ['btn btn-danger', '断开']
    '2': ['btn btn-warning', '欠压']
  updater = new Updater('pond_page', '/aquaculture/ajax/latest/') #全局更新器
  updater.add "pump_id:#{pump_id}", (data) ->
    pump_status = data
    $pump_status.html(status[pump_status][1])
    $pump_status.attr('class': status[pump_status][0])
    this

  # 日历
  $.fn.datepicker.defaults.language = "zh-CN"
  $.fn.datepicker.defaults.format = "yyyy-mm-dd"
  $('#start_time').datepicker()
  $('#end_time').datepicker()

  $table = $('table')
  # console.log $table
  $today_date = $('#today_date')
  setInterval ->
    now = (new Date()).strftime("%Y-%d-%m %H:%M:%S")
    $today_date.html(now)
    this
  , 1000

  tables =
    realtime: {}
    today: {}
    history: {}
  $table.each (key, tb)->
    # console.log tb.id
    id = tb.id
    # console.log id
    [tb, node_id, type] = id.split('_')
    tables[type][node_id] = new Table "##{id}"
  # console.log tables

  # ========================================
  # 绘制实时数据图表
  realtime_chart = ->
    for_realtime = (records) ->
      # 传入的是一个所有实时数据的
      # 针对只有2列的表格
      _records = []
      for record in records
        _record = {}
        _record['label'] = record[0]
        _record['value'] = parseFloat(record[1])
        _records.push(_record)
      return _records
      
    for key, table of tables.realtime
      el = table.el
      chart_name = tb_to_chart(el)
      data = for_realtime(table.records)
      elt = "#{chart_name} svg"
      # console.log elt
      # elt = "#chart_1 svg"

      ( (elt, data) ->
          nv.addGraph ->
            chart = nv.models.discreteBarChart()
              .x (d) ->
                # console.log d
                return d.label
              .y (d) ->
                return d.value
              .staggerLabels(true)
              .tooltips(false)
              .showValues(true)
            d = []
            d.push
              key: 'test'
              values: data
            # console.log elt, d
            d3.select(elt).datum(d)
            .transition().duration(500)
            .call(chart);

            nv.utils.windowResize(chart.update);

            return chart
        )(elt, data)
    this
  # realtime_chart()
  
  # ========================================
  # 监听显示今日图标按钮单击事件
  # 绘制今日数据图表
  today_chart = ->
    # alert 'today chart'
    cn_sensors = ['溶氧度', '温度']
    for_today = (records) ->
      # console.log records
      # 用于将数据放入每个记录条目中
      result = []
      for r in records
        time1 = r[0].replace /(.*)-(.*)-(.*)/g, "$1/$2/$3"
        time = new Date(time1)
        # console.log time
        # unix_time = time.getTime()
        result.push([time, parseFloat(r[1]), parseFloat(r[2])])
      # console.log result
      return result
    for key, table of tables.today
      el = table.el
      $table =  $(el)
      $show = $table.prev()
      chart_name = tb_to_chart(el)
      data = for_today(table.records)
      elt = "#{chart_name}"
      node_id = elt.split('_')[1]
      el = elt.slice(1)
      $el = document.getElementById(el)
      # console.log el, data
      g = new Dygraph $el, data,
        drawAxesZero: true
        includeZero: true
        labels: ['时间','溶氧度','温度']
        # legend: 'always'
        title: "节点#{node_id}今日数据"
        showRoller: true,
        rollPeriod: 14,
        # customBars: true,
        ylabel: ['溶氧度']
        y2label: ['温度 (C)']
        '温度':                 #第二轴
          axis: {}
      ( ($show, $table)->
        text = ['显示表格', '隐藏表格']
        pos = 0
        $show.click ->
          pos++
          $show.text(text[(pos%2)])
          $table.toggleClass('hide')
      )($show, $table)      
      # 自带更新器
      ( (node_id, g, data) ->
           # console.log node_id
           updater.add "node_id:#{node_id}", (latest) ->
             last_time = data[data.length - 1][0]
             d = []
             # console.log latest
             time1 = latest.shift()
             time1 = time1.replace /(.*)-(.*)-(.*)/g, "$1/$2/$3"
             time = new Date(time1)
             # console.log last_time, time
              
             if last_time.getTime() is time.getTime()
              # 如果时间一样，则什么都不做
               return
             # console.log time
             d.push(time)
             # d = data[node_id]
             for r in latest
               d.push(parseFloat(r[2]))
             # console.log d
             data.push(d)
             g.updateOptions( { 'file': data } );
             this
            this
        )(node_id, g, data)
    this

  today_chart()

  # 更新器
  setInterval ->
    updater.send()    
  , update_interval
  # updater.send()
  #
  # ===============================================
  # 绘制历史图表
  draw_history = () ->
    # alert 'today chart'
    cn_sensors = ['溶氧度', '温度']
    for_today = (records) ->
      # console.log records
      # 用于将数据放入每个记录条目中
      result = []
      for r in records
        time1 = r[0].replace /(.*)-(.*)-(.*)/g, "$1/$2/$3"
        time = new Date(time1)
        result.push([time, parseFloat(r[1]), parseFloat(r[2])])
      return result
    for key, table of tables.history
      el = table.el
      $table =  $(el)
      $show = $table.prev()
      chart_name = tb_to_chart(el)
      data = for_today(table.records)
      elt = "#{chart_name}"
      node_id = elt.split('_')[1]
      el = elt.slice(1)
      $el = document.getElementById(el)
      # console.log el, data
      g = new Dygraph $el, data,
        drawAxesZero: true
        includeZero: true
        labels: ['时间','溶氧度','温度']
        # legend: 'always'
        title: "节点#{node_id}历史数据"
        showRoller: true,
        rollPeriod: 14,
        # customBars: true,
        ylabel: ['溶氧度']
        y2label: ['温度 (C)']
        '温度':                 #第二轴
          axis: {}

      ( ($show, $table, node_id, g)->
        # 监听按钮事件
        # 显示/隐藏表格
        text = ['显示表格', '隐藏表格']
        pos = 0
        $show.click ->
          pos++
          $show.text(text[(pos%2)])
          $table.toggleClass('hide')
        # 图标菜单按钮
        $menu = $("#chart_#{node_id}_menu")
        pond_name = $menu.attr('pond_name')
        console.log $menu
        $menu.delegate 'button', 'click', (e) ->
          post = new Post '/aquaculture/ajax/history/', (data) ->
            # console.log data
            data = JSON.parse(data)
            # console.log data
            records = data[1]
            result = []
            for r in records
              time1 = r[0].replace /(.*)-(.*)-(.*)/g, "$1/$2/$3"
              time = new Date(time1)
              result.push([time, parseFloat(r[1]), parseFloat(r[2])])
            console.log result
            g.updateOptions( { 'file': result } );
          $button = $(e.target)
          time = $button.attr('time')
          if time is undefined
            $search = $button.parent()
            start = $search.find('input[name=start]').val()
            end = $search.find('input[name=end]').val()
            time ="#{start}:#{end}"
          alert time
          post.send
            time: time
            pond_name: pond_name
            node_id: node_id
      )($show, $table, node_id, g)
    this

  draw_history()
  return
