"use strict"
$ ->
  # alert 'here is setting'
  # ================================================
  # 视图层
  # addChainedAttributeAccessor = (obj, propertyAttr, attr) ->
  #   obj[attr] = (newValues...) ->
  #       if newValues.length == 0
  #           obj[propertyAttr][attr]
  #       else
  #           obj[propertyAttr][attr] = newValues[0]
  #           obj
  #
  # 订阅/发布系统
  subpub = ($) ->
    o = $({})
    $.subscribe = ->
      o.on.apply(o, arguments);
    $.unsubscribe = ->
      o.off.apply(o, arguments);
    $.publish = ->
      o.trigger.apply(o, arguments);
  subpub $

  $.subscribe '/error/', (e, error) ->
    alert error
  g_actions = ['add', 'remove', 'update']
  Backbone.sync = (action, model) ->
    # console.log @
    collection = @
    [method, ajax_url] = action.split('_')
    send_data = model.package(method)
    # console.log send_data
    post = new Post "/aquaculture/ajax/#{ajax_url}/",
      (data) ->
        console.log 'post', data
        try
          data = JSON.parse(data)
          # console.log data
          status = data.status
          if status is 200    #成功，error中为原信息
            alert '200'
            # $.publish url, [{status: 200, data: data.error}]
            if method isnt 'update'
              collection[method](model)
          else
            if action is 'update_pond'
              old_name = model.previous('pond_name')
              model.set {pond_name: old_name}
            $.publish '/error/', [{status: 500, error: '[Ponds.save]执行失败'}]
        catch error
          $.publish '/error/', [{status: 500, error: error}]
        this
    , (object, type, error) ->
      $.publish '/error/', [{status: 500, error: error}]
    # 数据总共有3种
    # 增加，删除和修改
    post.send
      mesg: JSON.stringify(send_data)
      # mesg: send_data
    this

  Node = Backbone.Model.extend
    default:
      pond_name: @pond_name || 'unknow'
      node_id: 'unknow'
    package: (method) ->
      if method in g_actions
        mesg =
          action: method
          data:
            pond_name: @get('pond_name')
            node_id: @get('node_id')
            old_id: @previous('node_id')
        console.log mesg
        return mesg

  NodeList = Backbone.Collection.extend
    model: Node
    comparator: "node_id"
  
  NodeView = Backbone.View.extend
    tagName: 'li'
    template: _.template '
      <label>节点编号：</label>
      <a href="/aquaculture/node/"><%= node_id %></a>
      <input class="update hide" type="" name="" value="<%= node_id %>" />
      <button class="remove btn btn-danger navbar-right" action="remove_node" node_id="<%= node_id %>">删除</button>'
    # </li>
    # <li node-id="<%= node_id %>">
    initialize: ()->
      @listenTo @model, "change", @render
      @listenTo @model, "remove", @remove
      
    render: ->
      html = @template @model.toJSON()
      @$el.html(html)
      this

    update: ->
      alert 'here is update'

  NodeListView = Backbone.View.extend
    # el: "##{@pond_name}_add_node"
    initialize: (data) ->
      @ids = data.ids
      pond = data.pond
      @pond_name = pond.get('pond_name')
      console.log @pond_name
      @el = "##{@pond_name}_add_node"
      @$el = $(@el)
      @$node_list = @$el.find('ul.node_list')
      @pond = pond
      @nodes = new NodeList

      @load()
      
      @listenTo @nodes, 'add', @add_one
      @listenTo @nodes, 'remove', @remove_one

      # $input = @$node_list.find('input:text')
      # $input.focus()

      _.bindAll(this, 'addEvent', 'removeEvent')
    events:
      "click button[action=add_node]": "addEvent"
      "click button[action=remove_node]": "removeEvent"
    load: ()->
      ids = @ids
      pond_name = @pond_name
      $node_list = @$node_list
      for id in ids
        node = new Node {node_id: id, pond_name: pond_name}
        node_view = new NodeView {model: node}
        $node_list.append(node_view.render().el)
        @nodes.add(node)
      
      # $node_list.children().each ->
        # console.log $(this)
    addEvent: (e) ->
      $button = $(e.target)
      $input = $button.prev()
      node_id = $input.val()
      nodes = @nodes
      # console.log nodes
      ids = nodes.pluck('node_id')
      if node_id is undefined or node_id in ids
        $.publish '/error/', ['无效节点编号']
        return
      node = new Node {node_id: node_id, pond_name: @pond_name}
      nodes.sync 'add_node', node
      # 测试
      # nodes.add node
      this
    removeEvent: (e) ->
      choice = confirm('你确定删除吗？')
      if !choice then return
      $button = $(e.target)
      $input = $button.prev()
      node_id = $input.val()
      pond_name = @pond_name
      nodes = @nodes
      r = nodes.where {node_id: node_id}
      # console.log r
      cid = r[0].cid
      model = nodes.get(cid)
      nodes.sync "remove_node", model
      # nodes.remove model
      # alert "here is remove node: #{node_id}, #{pond_name}"
    add_one: (node)->
      node_view = new NodeView {model: node}
      @$node_list.append(node_view.render().el)
      @nodes.add(node)
      
  Pond = Backbone.Model.extend
    default:
      pond_name: 'unknow'
      # old_name: 'unknow'
    package: (method) ->
      if method in g_actions
        mesg =
          action: method
          data:
            pond_name: @get('pond_name')
            old_name: @previous('pond_name')
      return mesg
    validate: (attrs, options) ->
      alert 'validate'
      if (!attrs.pond_name)
        $.publish '/error/', '无效的池塘名称'
        return
        # return 'error: invild pond name'
        
  PondList = Backbone.Collection.extend
    model: Pond
    comparator: "pond_name"
  
  ponds = new PondList
  ponds.bind 'error', ->
    alert 'error'
    # console.log arguments

  PondView = Backbone.View.extend
    tagName: "li"
    className: "col-lg-4"
    template: _.template '
      <div>
        <label>池塘的名称：</label>
        <a href="/aquaculture/pond/<%= pond_name %>" class="pond_name" pond_name="<%= pond_name %>"><%= pond_name %></a>
        <input class="update hide form-control" type="text" name="" value="<%= pond_name %>" />
        <button class="update btn btn-primary" action="update_pond" name="<%= pond_name %>">修改</button>
        <button class="remove btn btn-danger" action="remove_pond" name="<%= pond_name %>">删除</button>
      </div>
      <div id="<%= pond_name %>_add_node" class="node_list">
        <label>注册节点</label>      
        <input type="text" class="form-control" name="" value="" />
        <button class="add node btn btn-success" action="add_node">增加</button>
        <ul class="node_list"> </ul>
      </div>'
    # events:
    #   "click .remove": "remove"
    #   "click .update": "update"

    initialize: ->
      @listenTo @model, "change", @render
      @listenTo @model, "remove", @remove
      # @template = _.template @template
      # _.bindAll(this, 'remove', 'update')
    render: ->
      html = @template @model.toJSON()
      @$el.html(html)
      this
      
    update: ->
      alert 'here is update'
      
  PondListView = Backbone.View.extend
    el: "#pond_list_out"
    initialize: ->
      $el = @$el
      $add_pond = $('#add_pond')
      @$pond_list = $('#pond_list')
      @$input = $add_pond.find('input[type=text]')
      @$input.focus()

      # 在监听add事件之前加载，防止重复触发add事件
      @load()
      
      @listenTo ponds, 'add', @add_one
      @listenTo ponds, 'remove', @remove_one
      
      _.bindAll(this, 'addEvent', 'removeEvent', 'updateEvent') #保证在正确的上下文中执行
    events:
      "click button[action=add_pond]": "addEvent"
      "keypress #add_pond>input[type=text]": "addEvent"
      "click button[action=remove_pond]": "removeEvent"
      "click button[action=update_pond]": "updateEvent"
      # "keypress input.update": "updateEvent"
    addEvent: (e)->
      if e.button is 0 or e.keyCode is 13
        pond_name = @$input.val() || undefined
        # 当触发增加事件的时候就发一个ajax请求。
        # 如果执行成功，则更新模型。否则则报错
        names = ponds.pluck('pond_name')
        # console.log names
        if pond_name is undefined or pond_name in names 
          $.publish '/error/', ['无效池塘名称']
          return
        pond = new Pond {pond_name: pond_name}
        # alert pond_name
        ponds.sync 'add_pond', pond
        this
    removeEvent:(e) ->
      # console.log $(e)
      choice = confirm('你确定删除吗？')
      if !choice then return
      pond_name = $(e.target).attr('name')
      r = ponds.where {pond_name: pond_name}
      console.log r
      cid = r[0].cid
      model = ponds.get(cid)
      # console.log model
      ponds.sync 'remove_pond', model
      alert "here is my remove #{pond_name}"
    updateEvent: (e) ->
      $this = $(e.target)
      $input = $this.prev()
      $label = $input.prev()
      old_name = $label.attr('pond_name')
      if !$input.hasClass('hide')
        pond_name = $input.val()
        if pond_name isnt old_name
        # alert pond_name + '  ' + old_name
          r = ponds.where {pond_name: old_name}
          cid = r[0].cid
          model = ponds.get(cid)
          model.set {pond_name: pond_name}
          ponds.sync 'update_pond', model
      $input.toggleClass('hide')
      $label.toggleClass('hide')
      # alert 'here is my update'
    add_one: (pond) ->
      pond_view = new PondView {model: pond}
      @$pond_list.prepend(pond_view.render().el)
      node_list_view = new NodeListView
        pond: pond
        ids: []
    remove_one: (pond) ->
      pond.destroy()
    add_all: ->
      ponds.each @add_one, this
    load: ->
      # 用于载入已经渲染的池塘信息，解决历史遗留问题
      $pond_list = @$pond_list
      $pond_list.children().each ->
        pond_name = $(this).attr('pond_name')
        pond = new Pond {pond_name: pond_name}
        pond_view = new PondView {model: pond}
        $pond_list.append(pond_view.render().el)
        ids = []
        $(this).find('ul').children().each ->
          node_id = $(this).attr('node_id')
          ids.push(node_id)
          # node_list_view.load(node_id)
          # console.log node_id
        node_list_view = new NodeListView
          pond: pond
          ids: ids
        ponds.add(pond)
        $(this).remove()
  
  pond_list_view = new PondListView
  # console.log $("#pond2_add_node")
  #   alert 'hel'
# =================================================================
  
  # class Pond
  #   constructor: (@pond_name) ->
  #   save: (o) ->
  #     if !o
  #       $.publish '/pond/error', [{status: 500, error: '[Ponds.save]不是有效参数'}]
  #     action = o.action
  #     # alert "action=#{action}"
  #     switch action
  #       when "add"
  #         # alert 'add'
  #         url = '/pond/add'
  #       when "remove"
  #         url = '/pond/remove'
  #       when "update"
  #         url = '/pond/update'
  #       else
  #         url = '/'
  #     post = new Post '/aquaculture/ajax/pond/',
  #       (data) ->
  #         console.log 'post', data
  #         try
  #           data = JSON.parse(data)
  #           console.log data
  #           status = data.status
  #           if status is 200    #成功，error中为原信息
  #             alert '200'
  #             # alert url
  #             $.publish url, [{status: 200, data: data.error}]
  #           else
  #             $.publish '/pond/error', [{status: 500, error: '[Ponds.save]执行失败'}]
  #         catch error
  #           $.publish '/pond/error', [{status: 500, error: error}]
  #         this
  #     , (object, type, error) ->
  #       $.publish '/pond/error', [{status: 500, error: error}]
  #     # 数据总共有3种
  #     # 增加，删除和修改
  #     post.send
  #       mesg: JSON.stringify(o)
  #     this

  # class PondView
  #   el: '#pond_list'
  #   constructor: (@pond_name) ->
  #     @$el = $(@el)
  #     @$pond = $("li[pond-name=#{pond_name}]")
  #     @editing = false
  #     @model = new Pond
  #   template:'
  #   <li pond-name="<%= pond_name %>">
  #     <div>
  #       <label>池塘的名称：</label>
  #       <a href="/aquaculture/pond/<%= pond_name %>" class="pond_name"><%= pond_name %></a>
  #       <input class="update hide" type="text" name="" value="<%= pond_name %>" />
  #       <button class="update" action="update_pond" name="<%= pond_name %>">修改</button>
  #       <button class="remove" action="delete_pond" name="<%= pond_name %>">删除</button>
  #     </div>
  #     <p>绑定的节点编号：</p>
  #     <ul class="node_list">
  #       <p>现在还没有任何节点</p>
  #     </ul>
  #   </li>'
  #   load: ->
  #     pond_name = @pond_name
  #     $pond = @$pond
  #     model = @model
  #     # 绑定事件
  #     $pond.find('button[action="update_pond"]').on 'click',
  #       {model: model, action: "update", pond_view: this},
  #       @click
  #     $pond.find('button[action="delete_pond"]').on 'click',
  #       {model: model, action: "remove", pond_view: this},
  #       @click

  #     nodelistview = new NodeListView pond_name, '.node_list'
  #     nodelistview.load()
  #     this
  #   confirm: (text) ->
  #     confirm(text)
  #   click: (e) ->
  #     e.stopPropagation()
  #     e.preventDefault()
  #     model = e.data.model
  #     action = e.data.action
  #     pond_view = e.data.pond_view
  #     pond_name = $(this).attr('name')
  #     switch action
  #       when "remove"
  #         r = pond_view.confirm('你确定删除么?')
  #         if r
  #           o =
  #             action: 'remove'
  #             data:
  #               pond_name: pond_name
  #           model.save(o)
            
  #       when "update"
  #         if pond_view.editing
  #           old_name = pond_name
  #           new_name = $(this).prev().val()
  #           o =
  #             action: 'update'
  #             data:
  #               old_name: old_name
  #               new_name: new_name
  #           pond_view.editing = false
  #           model.save(o)
  #         else
  #           pond_view.editing = true
  #           pond_view.update()
  #       else
  #         $.publish '/pond/error', [{status: 500, error: '无效的动作'}]
  #     this
  #   add: ->
  #     pond_name = @pond_name
  #     $el = @$el
  #     @html = _.template @template, {pond_name: pond_name}
  #     $el.append(@html)        #插入DOM

  #     # 下面是绑定事件
  #     model = @model
  #     $pond = @$pond =  $("li[pond-name=#{pond_name}]") #这边必须刷新得到
  #     # 绑定事件
  #     $pond.find('button[action="update_pond"]').on 'click',
  #       {model: model, action: "update", pond_view: this},
  #       @click
  #     $pond.find('button[action="delete_pond"]').on 'click',
  #       {model: model, action: "remove", pond_view: this},
  #       @click
        
  #     nodelistview = new NodeListView pond_name, ".node_list"
  #     nodelistview.init()
  #     this
  #   remove: ->
  #     $pond = @$pond
  #     console.log $pond
  #     $pond.remove()
  #     this
  #   update: (new_name)->
  #     editing = @editing
  #     $pond = @$pond
  #     $label = $pond.find("a.pond_name")
  #     $edit = $pond.find("input.update_pond")
  #     if editing
  #       $edit.removeClass('hide')
  #       $label.addClass('hide')
  #     else
  #       $label.text(new_name)
  #       $pond.find('button[action="update_pond"]').attr('name', new_name)
  #       $pond.find('button[action="delete_pond"]').attr('name', new_name)
  #       $pond.find('button[action="add_node"]').attr('name', new_name)
  #       $label.removeClass('hide')
  #       $edit.addClass('hide')
  #     this
      
  # class PondListView
  #   constructor: (@el) ->
  #       @$el = $(@el)
  #       @ponds = {}             #用于存储每个池塘对象
  #       @model = new Pond      #和对像Ponds绑定
  #       # 订阅信道
  #       $.subscribe '/pond/error', $.proxy(@error, this)
  #       $.subscribe '/pond/add', $.proxy(@add, this)
  #       $.subscribe '/pond/remove', $.proxy(@remove, this)
  #       $.subscribe '/pond/update', $.proxy(@update, this)
  #       # 事件注册
  #       # 增加池塘的表单事件
  #       $add_pond = $('#add_pond')
  #       $add_pond.on 'submit', { model: @model, ponds: @ponds }, @submit
  #   submit: (event) ->
  #     # 表单提交事件
  #     model = event.data.model
  #     ponds = event.data.ponds
  #     event.stopPropagation()
  #     event.preventDefault()      #防止页面被刷新
  #     pond_name = $(this).find('input:first').val()
  #     # alert _.has(ponds, pond_name)
  #     if _.has(ponds, pond_name) #判读是否为重名
  #       a = 1
  #     else
  #       o =
  #         action: 'add'
  #         data:
  #           pond_name: pond_name
  #     model.save(o)
  #     return null
  #   load: () ->
  #     # 用于加载页面中已经存在的池塘
  #     $el = @$el
  #     ponds = @ponds
  #     model = @model
  #     $el.children().each ->
  #       pond_name = $(this).attr('pond-name')
  #       pondview = new PondView pond_name
  #       pondview.load()
  #       ponds[pond_name] = pondview #将其加入队列
  #     # console.log @ponds
  #   add: (e, mesg) ->
  #     data = mesg.data
  #     pond_name = data.pond_name
  #     pondview = new PondView pond_name
  #     pondview.add()
  #     @ponds[pond_name] = pondview #将其加入队列
  #     this
  #   remove: (e, mesg) ->
  #     data = mesg.data
  #     pond_name = data.pond_name
  #     @ponds[pond_name].remove()
  #     delete @ponds[pond_name]
  #     this
  #   update: (e, mesg)->
  #     data = mesg.data
  #     old_name = data.old_name
  #     new_name = data.new_name
  #     # alert old_name
  #     # alert new_name
  #     # console.log @ponds
  #     pond_view = @ponds[old_name]
  #     @ponds[new_name] = pond_view
  #     pond_view.update(new_name)
  #     delete @ponds[old_name]
  #     # console.log @ponds
  #     this
  #   error: (e, mesg) ->
  #     status = mesg.status
  #     error = mesg.error
  #     alert "Status: #{status}, Error: #{error}"
  #     this
      
  # class Node
  #   # constructor: () ->
  #   save: (o) ->
  #     if !o
  #       $.publish '/pond/error', [{status: 500, error: '[nodes.save]不是有效参数'}]
  #     action = o.action
  #     switch action
  #       when "add"
  #         url = '/node/add'
  #       when "remove"
  #         url = '/node/remove'
  #       when "update"
  #         url = '/node/update'
  #       else
  #         url = '/pond/error'
  #     alert action
  #     alert url
  #     post = new Post '/aquaculture/ajax/node/',
  #       (data) ->
  #         console.log 'post', data
  #         try
  #           data = JSON.parse(data)
  #           console.log data
  #           status = data.status
  #           if status is 200    #成功，error中为原信息
  #             alert '200'
  #             $.publish url, [{status: 200, data: data.error}]
  #           else
  #             $.publish '/pond/error', [{status: 500, error: '[Ponds.save]执行失败'}]
  #         catch error
  #           $.publish '/pond/error', [{status: 500, error: error}]
  #         this
  #     , (object, type, error) ->
  #       $.publish '/pond/error', [{status: 500, error: error}]

  #     # 数据总共有3种
  #     # 增加，删除和修改
  #     post.send
  #       mesg: JSON.stringify(o)
  #     this

  # class NodeListView
  #   load:  ->
  #     $el = @$el
  #     self = @
  #     $el.children().each ->
  #       node_id = $(this).attr('node-id')
  #       nodeview = new NodeView $el, node_id
  #       nodeview.load()
  #       self.nodes[node_id] = nodeview
  #     $add_node = $el.find("button[action=add_node]")
  #     console.log $add_node
  #     $add_node.on 'click', {model: @model, nodes: @nodes}, @click
  #     this      
  #   template:'
  #   <div class="add_node" >
  #     <input type="text" name="" value="" />
  #     <!-- 这里的name是池塘的名称，意思是归属哪个池塘 -->
  #     <button name="<%= pond_name %>" action="add_node">增加</button>
  #   </div>'
  #   constructor: (@pond_name, @el) ->
  #     el = "li[pond-name=#{pond_name}] #{@el}"
  #     @$el = $(el)
  #     @nodes = {}
  #     @model = new Node

  #     $.subscribe '/node/add', $.proxy(@add, this)
  #     $.subscribe '/node/remove', $.proxy(@remove, this)
  #     $.subscribe '/node/update', $.proxy(@update, this)

  #   init: () -> 
  #     html = _.template @template, {pond_name: @pond_name}
  #     @$el.append(html)
      
  #     add_node = @$el.find("button[action=add_node]")
  #     add_node.on 'click', {model: @model, nodes: @nodes}, @click
  #   click: (e) ->
  #     e.stopPropagation()
  #     e.preventDefault()
  #     model = e.data.model
  #     nodes = e.data.nodes
  #     node_id = $(this).prev().val()
  #     pond_name = $(this).attr('name')
  #     alert pond_name
  #     alert node_id
  #     o =
  #       action: 'add'
  #       data:
  #         node_id: node_id
  #         pond_name: pond_name
  #     model.save(o)
  #     return null
  #     this
  #   add: (e, mesg) ->
  #     data = mesg.data
  #     node_id = data.node_id
  #     nodeview = new NodeView @$el, node_id
  #     nodeview.add()
  #     @nodes[node_id] = nodeview
  #     this
  #   remove: (e, mesg) ->
  #     data = mesg.data
  #     node_id = data.node_id
  #     @nodes[node_id].remove()
  #     delete @nodes
  #     this
  # class NodeView
  #   load: () ->
  #     $el = @$el
  #     node_id = @node_id
  #     model = @model
  #     # alert node_id
  #     # console.log $el
  #     # $el.find("li[node-id=#{node_id}] button[action=update_node]").on 'click', {model: model, action: "update", node_view: this}, @click
  #     $el.find("li[node-id=#{node_id}] button[action=delete_node]").on 'click', {model: model, action: "remove", node_view: this}, @click
  #     this

  #     # <button class="update_node" action="update_node" name="<%= node_id %>">修改</button>
  #   template:'
  #   <li node-id="<%= node_id %>">
  #     <label>节点编号：</label>
  #     <a href="/aquaculture/node/"><%= node_id %></a>
  #     <input class="update_node hide" type="" name="" value="<%= node_id %>" />
  #     <button class="delete_node" action="delete_node" name="<%= node_id %>">删除</button>
  #   </li>'
  #   constructor: (@$el, @node_id) ->
  #     # el是增加到DOM的名称
  #     # pond_name是指隶属那个池塘
  #     # 需要在增加按钮上添加事件
  #     @node = @$el.find("li[node-id=#{@node_id}]")
  #     @model = new Node
  #     @editing = false
      
  #   add: ->
  #     html = _.template @template, {node_id: @node_id}
  #     @$el.append(html)
      
  #     model = @model
  #     node_id = @node_id
  #     # @$el.find("li[node-id=#{node_id}] button[action=update_node]").on 'click', {model: model, action: "update", node_view: this}, @click
  #     # console.log @$el
  #     @$el.find("li[node-id=#{node_id}] button[action=delete_node]").on 'click', {model: model, action: "remove", node_view: this}, @click
  #     this
      
  #   remove: ->
  #     $node = @node
  #     # console.log $node
  #     $node.remove()
  #     this
      
  #   confirm: (text) ->
  #     confirm(text)
  #   click: (e) ->
  #     e.stopPropagation()
  #     e.preventDefault()
  #     action = e.data.action
  #     model = e.data.model
  #     node_view = e.data.node_view
  #     node_id = $(this).attr('name')
  #     alert node_id
  #     switch action
  #       when "remove"
  #         r = node_view.confirm('你确定删除么？')
  #         if r
  #           o =
  #             action: 'remove'
  #             data:
  #               node_id: node_id
  #           model.save(o)
            
  # pondListView = new PondListView '#pond_list'
  # pondListView.load()           #用于加载前面被python渲染的池塘
  # return
  return 
