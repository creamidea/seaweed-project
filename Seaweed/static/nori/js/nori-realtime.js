// Generated by CoffeeScript 1.6.3
$(function() {
  var csrfSafeMethod, csrftoken, getCookie;
  getCookie = function(name) {
    var cookie, cookieValue, cookies, i, length, _i;
    cookieValue = null;
    if (document.cookie && document.cookie !== '') {
      cookies = document.cookie.split(';');
      length = cookies.length;
      for (i = _i = 0; 0 <= length ? _i < length : _i > length; i = 0 <= length ? ++_i : --_i) {
        cookie = jQuery.trim(cookies[i]);
        if (cookie.substring(0, name.length + 1) === (name + '=')) {
          cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
          break;
        }
      }
    }
    return cookieValue;
  };
  csrftoken = getCookie('csrftoken');
  csrfSafeMethod = function(method) {
    return /^(GET|HEAD|OPTIONS|TRACE)$/.test(method);
  };
  $.ajax({
    crossDomain: false,
    beforeSend: function(xhr, settings) {
      if (!csrfSafeMethod(settings.type)) {
        xhr.setRequestHeader("X-CSRFToken", csrftoken);
      }
    },
    url: '/nori/node/test/',
    type: 'POST',
    success: function(data) {
      console.log(data);
      console.log(JSON.parse(data));
    },
    fail: function(x) {
      console.log(x);
    }
  });
});
