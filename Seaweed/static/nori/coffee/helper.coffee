# 一些小魔法
print = console.log

# 各种执行函数
get_counter_from_table = (elt) ->
  counter = 0
  if elt
    $target = $("##{elt} tbody")
    counter = $target.children().length
  return counter
    
get_data_from_table = (elt) ->
  records = []
  if elt
    $target = $("##{elt} tbody tr")
    $target.each (key, tr)->
      record = []
      $(tr).children().each ->
        record.push(this.innerText)
        return
      records.push(record)
      return
  return records

class Data
  today: (records) ->
    options = {}
    if records
      data = []
      sensor = {}
      pos = {}                    #用于记录数据是在data中的什么位置
      for record in records
        r =
          time: null
          '101': null
          '102': null
          '103': null
          '104': null
        time = record[3]
        value = record[2]
        name = record[1]
        id = record[0]
        if pos[time] is undefined
          r.time = time
          # r.time = parse_time time
          r[id] = value
          pos[time] = data.length
          data.push r
        else
          data[pos[time]][id] = value
      options =
        xkey: 'time'
        ykeys: ['101', '102', '103', '104']
        labels: ['光照', '温度', '流速', '浊度']
        data: data
    return options
  # 准备实时数据
  # 以传感器作为x轴，以数据为y轴的数据整合
  realtime: (records) ->
    options = {}
    if records
      labels = []
      data = []
      for record in records
        # console.log record
        labels.push record[3]
        data.push({'name': record[1], 'value': record[2]})
    options =
      xkey: 'name'
      ykeys: ['value']
      labels: labels
      data: data
    return options

draw_chart = (from, to, type) ->
  if typeof(from) is 'string' and
     typeof(to) is 'string' and
     type in ['Bar', 'Line']
    records = get_data_from_table from
    if records.length > 0
      try
        handle = to.split('_')[0]
      catch error
        return
      data = new Data
      if handle is 'history'
        handle = 'today'
      options = data[handle] records
      # options.pointSize = 0
      options.element = to
      # options.hoverCallback = (index, options, content) ->
        # row = options.data[index]
        # "sin(#{row.x})=#{row.y}"
        # console.log "index=#{index}, options=#{options}, content=#{content}"
      Morris[type] options
    else
      $('#'+to).html("现在还没有数据")
  return

en_months =
  'Jan': '01'
  'Feb': '02'
  'Mar': '03'
  'Apr': '04'
  'May': '05'
  'Jun': '06'
  'Jul': '07'
  'Aug': '08'
  'Sep': '09'
  'Oct': '10'
  'Nov': '11'
  'Dec': '12'

cn_months =
  '一月': '01'
  '二月': '02'
  '三月': '03'
  '四月': '04'
  '五月': '05'
  '六月': '06'
  '七月': '07'
  '八月': '08'
  '九月': '09'
  '十月': '10'
  '十一月': '11'
  '十二月': '12'

months = en_months

parse_time = (time) ->
  rlt = ""
  # console.log time
  if time
    t = time.split(', ')
    month_days = t[0].split('. ')
    month = months[month_days[0]]
    day = month_days[1]
    year = t[1]
    point = t[2].split(' ')
    if point[1] is "p.m."
      hour = point[0].split(':')[0]
      min = point[0].split(':')[1] or '00'
      hour = parseInt(hour, 10) + 12
      point = hour + ':' + min
    else if point[1] is "a.m."
      point = point[0]
      if point.length < 3       #防止出现如4这种情况，应该将其改为4:00
        point += ':00'
    rlt = "#{year}-#{month}-#{day} #{point}"
    # console.log rlt
  return rlt

##############################################################
# 绘制图形，屏蔽底层使用的图形库
class Draw
  constructor: (@id) ->
  parse: (data) ->
    @parsed_data = data
  # 加载数据
  load: (data) ->
    @origin_data = data
  # 绘制图形
  draw: (to) ->
    @to = to
  # 销毁图形
  destroy: () ->
    
# 使用Dygraphs图表库绘制图表
class DrawDygraphs extends Draw
  constructor: (@id) ->
    super @id
  # 图形类内部数据解析器
  parse: () ->
    data = @origin_data
    result = []
    for k, d of data
      # console.log "key is #{k}, value is #{d}"
      d.unshift(new Date(k))
      result.push(d)
    super result
      
  load: (data) ->
    self = @
    super data                  #存入类内部，成为其内部属性
    self.parse()
  range: ->
    min = []
    max = []
    data = @origin_data
    for k, v of data
      a = v.slice(1)
      min.push(_.min(a))
      max.push(_.max(a))
    range =
        min: _.min(min)
        max: _.max(max)
    console.log range
  draw: (to)->
    data = @parsed_data
    range = @range()
    @g = new Dygraph $("##{to}")[0], data,
      labels: ["x", "101", "102", "103", "104"]
      includeZero: true
      drawAxesZero: true
      # valueRange: [range.min, range.max]

  destroy: ->
    @g.destroy()

##############################################################

##############################################################
# 数据解析器
# 屏蔽不同的数据来源
class Parse
  # id是标识符
  constructor: (@id) ->

  # 用于读取数据
  read: (records) ->
    @records = records
    return this
    
  # 用于解析数据
  parse: () ->
    return this
    
# 解析来自表格的数据
class ParseTable extends Parse
  constructor: (@id) ->
    super @id

  # 读取来自表格的数据，
  # 返回数组，每个item为其每个属性组成的数组
  read: (@from)->
    records = []
    elt = @from
    $target = $("##{elt} tbody tr")
    $target.each (key, tr)->
      record = []
      $(tr).children().each ->
        record.push(this.innerText)
        return
      records.push(record)
      return
    super records               #存入对象内部属性
    
  # 返回值是一个对象
  # 以时间为键，以每个传感器采集在此时间点上的采集值为值
  parse: () ->
    records = @records
    items = {}                   #以时间为键，以每个传感器采集在此时间点上的采集值为值
    for record in records
      time = record[3]
      # time = parse_time(record[3])
      if items[time] is undefined
        items[time] = new Array(4) #这里硬编码成4个，主要是4中传感器。
        
      # **Attention:**
      # 这里直接硬编码了传感器ID，不知道后期有没有更好的解决方式
      switch record[0]
        when "101" then items[time][0] = parseFloat(record[2])
        when "102" then items[time][1] = parseFloat(record[2])
        when "103" then items[time][2] = parseFloat(record[2])
        when "104" then items[time][3] = parseFloat(record[2])
    return items
##############################################################

##############################################################
# Ajax  请求
class Ajax
  constructor: (@url, @success, @fail) ->
  getCookie: (name) ->
      cookieValue = null
      if (document.cookie && document.cookie != '')
          cookies = document.cookie.split(';')
          length = cookies.length
          for i in [0...length]
              cookie = jQuery.trim(cookies[i])
              if cookie.substring(0, name.length+1) == (name + '=')
                  cookieValue = \
                    decodeURIComponent(cookie.substring(name.length+1))
                  break
      return cookieValue;
  csrfSafeMethod: (method) ->
      # these HTTP methods do not require CSRF protection
      (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));

  beforeSend: (xhr, settings) =>
      if (!@csrfSafeMethod(settings.type))
          xhr.setRequestHeader("X-CSRFToken", @getCookie('csrftoken'))
      return
  crossDomain: false
  type: 'POST'
  # success: (data) ->
  #     if typeof(fial) is 'function'
  #       success(data)
  #     else
  #       return null
  # fail: (jqXHR, textStatus, errorThrown)->
  #     if typeof(fial) is 'function'
  #       return fail(jqXHR, textStatus, errorThrown)
  #     else
  #       console.log jqXHR, textStatus, errorThrown 
  send: (data)->
    # console.log "Sending..."
    try
      $.ajax
        crossDomain: @crossDomain
        beforeSend: @beforeSend
        url: @url
        type: @type
        data: data
        success: @success
        fail: @fail
    catch error
      return
    return
    
class Post extends Ajax
  type: 'POST'
  send: (data)->
    super(data)
    
class Get extends Ajax
  type: 'GET'
  send: (data)->
    super(data)
##############################################################
