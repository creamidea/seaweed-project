$ ->
    getCookie = (name) ->
        cookieValue = null
        if (document.cookie && document.cookie != '')
            cookies = document.cookie.split(';')
            length = cookies.length
            for i in [0...length]
                cookie = jQuery.trim(cookies[i])
                if cookie.substring(0, name.length+1) == (name + '=')
                    cookieValue = decodeURIComponent(cookie.substring(name.length+1))
                    break
        return cookieValue;

    csrftoken = getCookie('csrftoken')

    csrfSafeMethod = (method) ->
        # these HTTP methods do not require CSRF protection
        (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
        
    $.ajax
        crossDomain: false
        beforeSend: (xhr, settings) ->
            if (!csrfSafeMethod(settings.type))
                xhr.setRequestHeader("X-CSRFToken", csrftoken);
            return
        url: '/nori/node/test/'
        type: 'POST'
        success: (data) ->
            console.log data
            console.log JSON.parse(data)
            return
        fail: (x) ->
            console.log x
            return
    return


# ///////////////////////////////////////////

