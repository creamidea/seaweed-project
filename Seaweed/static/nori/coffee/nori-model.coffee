# 传感器
class Sensor
  constructor:(@id, @name, @unit) ->
  

class LightSensor extends Sensor
  constructor: (@data) ->
    super "101", "light", "lux"

# 节点对象
class Node
  constructor: (@data) ->
    return

  test: ->
    console.log @data


class Animal
  constructor: (@name) ->

  move: (meters) ->
    alert @name + " moved #{meters}"

class Snake extends Animal
  move: ->
    alert "Slithering..."
    super 5

# sam = new Snake "Sammy the Python"
# alert sam.name
  
