# -*- encoding: utf-8 -*-

# 从djangp.conf.urls导入patterns, include and url
from django.conf.urls import patterns, include, url

# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()

from django.contrib.auth.views import logout, password_change_done

from django.views.generic import TemplateView, RedirectView
# from django.views.generic.simple import direct_to_template # deprecated
from .views import my_login, my_password_change, register, user_profile_view, create_playlist, test

class TextPlainView(TemplateView):
    def render_to_response(self, context, **kwargs):
        return super(TextPlainView, self).render_to_response(
            context, content_type='text/plain', **kwargs)

urlpatterns = patterns('',
                       # (r'^searchform/$', views.search_form),
                       (r'^about/$',
                        TemplateView.as_view(template_name='about.html')),

                       url(r'^$',
                           'Seaweed.views.home',
                           name='home'),

                       (r'^favicon\.ico$',
                        RedirectView.as_view(url='/static/img/favicon.ico')),
                       url(r'^robots\.txt$',
                           TextPlainView.as_view(template_name='robots.txt')),

                       # Account 
                       url(r'^accounts/login/$',
                           my_login,
                           name='my_login'),
                       url(r'^accounts/logout/$',
                           logout,
                           {'template_name': "registration/logout.html"},
                           name='my_logout'),
                       url(r'^accounts/profile/$',
                           user_profile_view,
                           name='my_profile'),
                       # url(r'^accounts/profile/$', TemplateView.as_view(template_name='registration/profile.html'), name='my_profile'),
                       url(r'^accounts/register/$',
                           register,
                           name='my_register'),
                       url(r'^accounts/password/change/$',
                           my_password_change,
                           name='my_password_change'),
                       url(r'^accounts/password/change/done/$',
                           password_change_done,
                           name='my_password_change_done'),
                       # (r'^$', TemplateView.as_view(template_name='index.html')),
    # url(r'^Seaweed/', include('Seaweed.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
                       # url(r'^admin/', include(admin.site.urls)),
)

# test
urlpatterns += patterns('', 
                        (r'^playlist/create/$',
                         create_playlist),
                        (r'^test/$', test),
                        )
# For pond
# 水产项目
urlpatterns += patterns('',
                        (r'^aquaculture/', include('Seaweed.aquaculture.urls')),
)
# 紫菜项目
urlpatterns += patterns('',
                        (r'^nori/', include('Seaweed.nori.urls')),
)


